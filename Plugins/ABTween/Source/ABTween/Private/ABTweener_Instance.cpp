//// Fill out your copyright notice in the Description page of Project Settings.
//
//
#include "ABTweener_Instance.h"
#include "ABTweener_Object.h"

//Creiamo i vari setter di tutte le modifiche aggiuntive dei nostri tween
UABTweener_Instance* UABTweener_Instance::SetDelay(float InDelaySecs)
{
	this->DelaySecs = InDelaySecs;
	return this;
}

UABTweener_Instance* UABTweener_Instance::SetLoops(int InNumLoops)
{
	this->NumLoops = InNumLoops;
	return this;
}

UABTweener_Instance* UABTweener_Instance::SetLoopDelay(float InLoopDelaySecs)
{
	this->LoopDelaySecs = InLoopDelaySecs;
	return this;
}

UABTweener_Instance* UABTweener_Instance::SetYoyo(bool bInShouldYoyo)
{
	this->bShouldYoyo = bInShouldYoyo;
	return this;
}

UABTweener_Instance* UABTweener_Instance::SetYoyoDelay(float InYoyoDelaySecs)
{
	this->YoyoDelaySecs = InYoyoDelaySecs;
	return this;
}

UABTweener_Instance* UABTweener_Instance::SetTimeMultiplier(float InTimeMultiplier)
{
	this->TimeMultiplier = FMath::Abs(InTimeMultiplier);
	return this;
}

UABTweener_Instance* UABTweener_Instance::SetCanTickDuringPause(bool bInCanTickDuringPause)
{
	this->bCanTickDuringPause = bInCanTickDuringPause;
	return this;
}

UABTweener_Instance* UABTweener_Instance::SetGlobalTimeDilatation(bool bInUseGlobalTimeDilatation)
{
	this->bUseGlobalTimeDilatation = bInUseGlobalTimeDilatation;
	return this;
}

UABTweener_Instance* UABTweener_Instance::SetAutoDestroy(bool bInShouldAutoDestroy)
{
	this->bShouldAutoDestroy = bInShouldAutoDestroy;
	return this;
}

UABTweener_Instance* UABTweener_Instance::SetOnYoyo(TFunction<void()> Handler)
{
	this->OnYoyo = MoveTemp(Handler);
	return this;
}

UABTweener_Instance* UABTweener_Instance::SetOnLoop(TFunction<void()> Handler)
{
	this->OnLoop = MoveTemp(Handler);
	return this;
}

UABTweener_Instance* UABTweener_Instance::SetOnCompleted(TFunction<void()> Handler)
{
	this->OnCompleted = MoveTemp(Handler);
	return this;
}

//Inizializzamo tutti i nostri valori
void UABTweener_Instance::InitializeSharedMembers(float InDurationInSecs, ETweenEase InEaseType)
{
	//Controlliamo se la durata � minore o uguale a zero. Se lo � gli diamo una durata scriptata molto molto bassa.
	//Se invece non lo � gli settiamo il valore dato
	if (InDurationInSecs <= 0)
		this->DurationInSecs = .001f;
	else
		this->DurationInSecs = InDurationInSecs;

	//Inizializzamo la Ease
	this->EaseType = InEaseType;

	//Inizialiizamo i booleani e i valori numerici
	Counter = 0;
	DelayCounter = 0;
	bShouldAutoDestroy = true;
	bIsActive = true;
	bIsPaused = false;
	bShouldYoyo = false;
	bIsPlayingYoyo = false;
	bCanTickDuringPause = false;
	bUseGlobalTimeDilatation = true;

	NumLoops = 1;
	NumLoopCompleted = 0;
	DelaySecs = 0;
	LoopDelaySecs = 0;
	YoyoDelaySecs = 0;
	TimeMultiplier = 1.f;

	//Inizializzamo il nostro stato di delay
	DelayState = EDelayState::None;

	//Inizializzamo le nostre funzioni generiche
	OnYoyo.Reset();
	OnLoop.Reset();
	OnCompleted.Reset();
}

//Facciamo partire i tween
void UABTweener_Instance::Start()
{
	//Settiamo il DelayCounter come il DelaySecs
	DelayCounter = DelaySecs;
	//adesso controlliamo se il DelayCounter � maggiore di zero. Se lo � settiamo il DelayState a Start altrimenti lo settiamo a None
	if (DelayCounter > 0)
		DelayState = EDelayState::Start;
	else
		DelayState = EDelayState::None;
}

//Facciamo Ripartire i tween
void UABTweener_Instance::Restart()
{
	//Controlliamo se i tween sono attivi. Se lo sono resettiamo i valori di Counter, il bool del PlayYoyo e dei Loop completati, si toglie la pausa e lo si fa partire.
	if (bIsActive)
	{
		Counter = 0;
		bIsPlayingYoyo = false;
		NumLoopCompleted = 0;
		Unpause();
		Start();
	}
}

void UABTweener_Instance::Destroy()
{
	//Settiamo a false il booleano per sapere se il tween � attivo
	bIsActive = false;
	//Resettiamo tutte le funzioni TFunction
	OnYoyo.Reset();
	OnLoop.Reset();
	OnCompleted.Reset();
}

UABTweener_Object* UABTweener_Instance::CreateObject(UObject* Outer)
{
	//Creaimo un nuovo oggetto
	UABTweener_Object* Wrapper = NewObject<UABTweener_Object>(Outer);
	//Gli assegnamo questa classe come tweener intance
	Wrapper->SetTweenerInstance(this);
	//e per finire ritorniamo l'oggetto
	return Wrapper;
}

void UABTweener_Instance::Pause()
{
	//Settiamo a vero il booleano della pausa
	bIsPaused = true;
}

void UABTweener_Instance::Unpause()
{
	//Settiamo a false il booleano della pausa
	bIsPaused = false;
}

void UABTweener_Instance::Update(float UnscaleDeltaSeconds, float DilatedDeltaSeconds, bool bIsGamePaused)
{
	//controlliamo se il gioco � pausato oppure non � attivo oppure se il fatto che il gioco sia in pausa e che NON possa tickare durante la pausa.
	//se uno di questi controlli non viene superato non fa nulla
	if (bIsPaused || !bIsActive || bIsGamePaused && !bCanTickDuringPause) return;

	//Creiamo un float al quale assegnamo che tipo di DeltaSeconds dobbiamo utilizzare e lo moltiplichiamo per il TimeMultiplier.
	float DeltaTime = bUseGlobalTimeDilatation ? DilatedDeltaSeconds : UnscaleDeltaSeconds;
	DeltaTime *= TimeMultiplier;

	//Controlliamo il DelayCounter � maggiore di zero.
	if (DelayCounter > 0)
	{
		//Se lo � iniziamo a farlo scendere sottraendogli il nostro float DeltaTime creato sopra.
		DelayCounter -= DeltaTime;
		//Adesso controlliamo se il DelayCounter � minore o uguale a zero.
		if (DelayCounter <= 0)
		{
			//Se lo � facciamo un cambio di stato
			switch (DelayState)
			{
			case EDelayState::Loop:
				if (OnLoop)
					OnLoop();
				break;
			case EDelayState::Yoyo:
				if (OnYoyo)
					OnYoyo();
				break;
			}
		}
	}
	else //altrimenti se il DelayCounter � miniore di zero
	{
		//controlliamo se sta eseguendo lo Yoyo. Se lo esegue sottraiamo il Counter con il DeltaTime altrimenti lo sommiamo.
		if (bIsPlayingYoyo)
		{
			Counter -= DeltaTime;
		}
		else
		{
			Counter += DeltaTime;
		}

		//Poi clampiamo il Counter tra se stesso e zero in una durata.
		Counter = FMath::Clamp<float>(Counter, 0, DurationInSecs);

		//Facciamo partire la funzione che applica le Ease.
		ApplyEasing(UABTweener_Ease::Ease(EaseType, Counter / DurationInSecs));

		//Poi controlliamo ancora se sta eseguendo lo Yoyo.
		if (bIsPlayingYoyo)
		{
			//se lo fa e se il counter � minore o uguale a zero completa il loop.
			if (Counter <= 0)
			{
				CompleteLoop();
			}
		}
		else
		{
			//Se invece il counter � maggiore o uguale alla DurationinSecs.
			if (Counter >= DurationInSecs)
			{
				//Controlliamo se deve far andare lo Yoyo.
				if (bShouldYoyo)
				{
					//Se vero fa partire lo Yoyo.
					StartYoyo();
				}
				else
				{
					//altrimenti completa il loop.
					CompleteLoop();
				}
			}
		}
	}
}

void UABTweener_Instance::CompleteLoop()
{
	//Aggiungiamo in coda un nuovo loop completato.
	++NumLoopCompleted;
	//poi controlliamo se il numero dei loop � minore di zero oppure se il numero dei loop completati � minore del numero dei loop. Se lo sono si fa partire un nuovo loop.
	if (NumLoops < 0 || NumLoopCompleted < NumLoops)
	{
		StartNewLoop();
	}
	else
	{
		//altrimenti per prima cosa controlliamo se il tutto � stato completato.
		if (OnCompleted)
		{
			//Se vero lo confermiamo.
			OnCompleted();
		}

		//poi controlliamo se deve essere distrutto.
		if (bShouldAutoDestroy)
		{
			//Se vero lo distruggiamo.
			Destroy();
		}
		else
		{
			//Altrimenti lo mettiamo in pausa.
			Pause();
		}
	}
}

void UABTweener_Instance::StartNewLoop()
{
	//Per prima cosa assegnamo il DelayCounter uguale al Delay in Secondi del Loop.
	DelayCounter = LoopDelaySecs;
	//Poi resettiamo il valore dei Counter.
	Counter = 0;
	//E settiamo a false il fatto che debba fare lo Yoyo
	bIsPlayingYoyo = false;

	//Poi controlliamo se il delay counter � maggiore di zero
	if (DelayCounter > 0)
	{
		//Se lo � sllora settiamo lo stato attivo come Loop
		DelayState = EDelayState::Loop;
	}
	else
	{
		//altrimenti controlliamo se � in loop e lo confermiamo
		if (OnLoop)
		{
			OnLoop();
		}
	}
}

void UABTweener_Instance::StartYoyo()
{
	//Settiamo a true il fatto che deve fare lo Yoyo.
	bIsPlayingYoyo = true;
	//Poi Settiamo il valore di DelayCounter uguale al Delay in Secondi dello Yoyo
	DelayCounter = YoyoDelaySecs;

	//Poi controlliamo se il delay counter � maggiore di zero
	if (DelayCounter > 0)
	{
		//Se lo � allora settiamo lo stato attivo come Yoyo
		DelayState = EDelayState::Yoyo;
	}
	else
	{
		//altrimenti controlliamo se � in yoyo e lo confermiamo
		if (OnYoyo)
		{
			OnYoyo();
		}
	}
}