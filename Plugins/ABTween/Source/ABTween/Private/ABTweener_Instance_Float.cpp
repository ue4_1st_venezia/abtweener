// Fill out your copyright notice in the Description page of Project Settings.


#include "ABTweener_Instance_Float.h"

void UABTweener_Instance_Float::Initialize(float InStart, float InEnd, TFunction<void(float)> InOnUpdate, float InDurationSecs, ETweenEase InEaseType)
{
	this->StartValue = InStart;
	this->EndValue = InEnd;
	this->OnUpdate = MoveTemp(InOnUpdate);
	this->InitializeSharedMembers(InDurationSecs, InEaseType);
}

void UABTweener_Instance_Float::ApplyEasing(float EasedPercent)
{
	OnUpdate(FMath::Lerp(StartValue, EndValue, EasedPercent));
}
