// Fill out your copyright notice in the Description page of Project Settings.


#include "ABTweener_Subsystem.h"
#include "ABTweener_Module.h"
#include "Kismet/GameplayStatics.h"

void UABTweener_Subsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
	//all'inizializzazione mettiamo il LastTIckedFrame uguale al frame counter dell'engine.
	LastTickedFrame = GFrameCounter;

	//poi contolliamo con una macro se questo parte solo con l'editor. Se lo fa puliamo tutti i tween.
#if WITH_EDITOR
	UABTweener_Module::ClearActiveTweens();
#endif
}

void UABTweener_Subsystem::Deinitialize()
{
	Super::Deinitialize();
	//alla deinizializzazione contolliamo con una macro se questo parte solo con l'editor. Se lo fa facciamo un check della capienza dei tween e poi li puliamo.
#if WITH_EDITOR
	UABTweener_Module::CheckTweenCapacity();
	UABTweener_Module::ClearActiveTweens();
#endif
}

void UABTweener_Subsystem::Tick(float DeltaTime)
{
	//Per prima cosa controlliamo se il nostro LastTickedFrame � minore al frame counter dell'engine.
	if (LastTickedFrame < GFrameCounter)
	{
		//se lo � mettiamo il LastTIckedFrame uguale al frame counter dell'engine.
		LastTickedFrame = GFrameCounter;

		//Poi controlliamo se il Mondo � diverso da nullptr
		if (GetWorld() != nullptr) //(se lo� qualche problema c'�)
		{
			//Se non lo � facciamo l'update
			UABTweener_Module::Update(GetWorld()->DeltaRealTimeSeconds, GetWorld()->DeltaTimeSeconds, GetWorld()->IsPaused());
		}
	}
}

ETickableTickType UABTweener_Subsystem::GetTickableTickType() const
{
	//Qui ritorniamo il tipo di tick che si vuole utilizzare
	return ETickableTickType::Always; 
}

TStatId UABTweener_Subsystem::GetStatId() const
{
	//CICLO DI DICHIARAZIONE RAPIDO DI RITORNO STATISTICA
	//Non ho capito cosa sia ma secondo la documentazione: Restituisce un contatore di cicli e talvolta viene utilizzato da alcune classi specializzate.
	RETURN_QUICK_DECLARE_CYCLE_STAT(UABTweener_Module, STATGROUP_Tickables);
}

bool UABTweener_Subsystem::IsTickableWhenPaused() const
{
	return true;
}

bool UABTweener_Subsystem::IsTickableInEditor() const
{
	return false;
}
