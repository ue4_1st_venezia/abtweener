// Fill out your copyright notice in the Description page of Project Settings.


#include "ABTweener_Object.h"

UABTweener_Object::UABTweener_Object()
{
	//Rendiamo nullptr la nostra istanza
	Tween = nullptr;
}

void UABTweener_Object::BeginDestroy()
{
	//Controlliamo se la nostra istanza � diversa da nullptr
	if (Tween != nullptr)
	{
		//se lo � la distruggiao e la rendiamo nullptr
		Tween->Destroy();
		Tween = nullptr;
	}
	//Poi chiamiamo questa funzione che consente all'oggetto di iniziare un processo di pulizia asincrono.
	UObject::BeginDestroy();
}

void UABTweener_Object::SetTweenerInstance(UABTweener_Instance* InTween)
{
	//Qui assegnamo l'istanza come quella in input 
	this->Tween = InTween;
	//E poi assegnamo a false il set dell'auto distruzione
	this->Tween->SetAutoDestroy(false);
}

void UABTweener_Object::Destroy()
{
	//distruggiamo l'istazna e la rendiamo nullptr
	this->Tween->Destroy();
	this->Tween = nullptr;
	//Poi chiamiamo questa funzione che consente all'oggetto di iniziare un processo di pulizia asincrono.
	ConditionalBeginDestroy();
}
