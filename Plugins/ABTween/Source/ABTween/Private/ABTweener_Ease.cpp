// Fill out your copyright notice in the Description page of Project Settings.


#include "ABTweener_Ease.h"

#pragma region Ease
float UABTweener_Ease::Ease(ETweenEase EaseType, float Time)
{
	switch (EaseType)
	{
	case ETweenEase::None:
		return 0;
		break;
	case ETweenEase::EaseLinear:
		return EaseLinear(Time);
		break;
	case ETweenEase::EaseSmoothstep:
		return EaseSmoothstep(Time, 0, 1);
		break;
	case ETweenEase::EaseInSin:
		return EaseInSin(Time);
		break;
	case ETweenEase::EaseOutSine:
		return EaseOutSine(Time);
		break;
	case ETweenEase::EaseInOutSine:
		return EaseInOutSine(Time);
		break;
	case ETweenEase::EaseInCubic:
		return EaseInCubic(Time);
		break;
	case ETweenEase::EaseOutCubic:
		return EaseOutCubic(Time);
		break;
	case ETweenEase::EaseInOutCubic:
		return EaseInOutCubic(Time);
		break;
	case ETweenEase::EaseInQuint:
		return EaseInQuint(Time);
		break;
	case ETweenEase::EaseOutQuint:
		return EaseOutQuint(Time);
		break;
	case ETweenEase::EaseInOutQuint:
		return EaseInOutQuint(Time);
		break;
	case ETweenEase::EaseInCirc:
		return EaseInCirc(Time);
		break;
	case ETweenEase::EaseOutCirc:
		return EaseOutCirc(Time);
		break;
	case ETweenEase::EaseInOutCirc:
		return EaseInOutCirc(Time);
		break;
	case ETweenEase::EaseInElastic:
		return EaseInElastic(Time);
		break;
	case ETweenEase::EaseOutElastic:
		return EaseOutElastic(Time);
		break;
	case ETweenEase::EaseInOutElastic:
		return EaseInOutElastic(Time);
		break;
	case ETweenEase::EaseInQuad:
		return EaseInQuad(Time);
		break;
	case ETweenEase::EaseOutQuad:
		return EaseOutQuad(Time);
		break;
	case ETweenEase::EaseInOutQuad:
		return EaseInOutQuad(Time);
		break;
	case ETweenEase::EaseInQuart:
		return EaseInQuart(Time);
		break;
	case ETweenEase::EaseOutQuart:
		return EaseOutQuart(Time);
		break;
	case ETweenEase::EaseInOutQuart:
		return EaseInOutQuart(Time);
		break;
	case ETweenEase::EaseInExpo:
		return EaseInExpo(Time);
		break;
	case ETweenEase::EaseOutExpo:
		return EaseOutExpo(Time);
		break;
	case ETweenEase::EaseInOutExpo:
		return EaseInOutExpo(Time);
		break;
	case ETweenEase::EaseInBack:
		return EaseInBack(Time);
		break;
	case ETweenEase::EaseOutBack:
		return EaseOutBack(Time);
		break;
	case ETweenEase::EaseInOutBack:
		return EaseInOutBack(Time);
		break;
	case ETweenEase::EaseInBounce:
		return EaseInBounce(Time);
		break;
	case ETweenEase::EaseOutBounce:
		return EaseOutBounce(Time);
		break;
	case ETweenEase::EaseInOutBounce:
		return EaseInOutBounce(Time);
	}

	return 0.0f;
}
#pragma endregion

#pragma region Ease Linear and Smoothstep
float UABTweener_Ease::EaseLinear(float Time)
{
	return Time;
}

float UABTweener_Ease::EaseSmoothstep(float Time, float Param1, float Param2)
{
	if (Time < Param1)
	{
		return 0;
	}
	else if (Time >= Param2)
	{
		return 1;
	}
	else
	{
		Time = (Time - Param1) / (Param2 - Param1);
		return Time * Time * (3 - 2 * Time);
	}
}
#pragma endregion

#pragma region Ease Sin
float UABTweener_Ease::EaseInSin(float Time)
{
	//cos(-x*3.14)/2
	return 1 - FMath::Cos((Time * Pi) / 2);
}

float UABTweener_Ease::EaseOutSine(float Time)
{
	//sin(x*3.14)/2
	return FMath::Sin((Time * Pi) / 2);
}

float UABTweener_Ease::EaseInOutSine(float Time)
{
	//cos((-3,14*x)-1)/2
	return -(FMath::Cos(Pi * Time) - 1) / 2;
}
#pragma endregion

#pragma region Ease Cubic
float UABTweener_Ease::EaseInCubic(float Time)
{
	float CubedTime = Time * Time * Time;
	//x*x*x
	return CubedTime;
}

float UABTweener_Ease::EaseOutCubic(float Time)
{
	//1 - (1 - x) ^ 3
	return 1 - FMath::Pow(1 - Time, 3);
}

float UABTweener_Ease::EaseInOutCubic(float Time)
{
	float CubedTime = Time * Time * Time;

	if (Time < .5f)
	{
		//4*x*x*x
		return 4 * CubedTime;
	}
	else
	{
		//1 - (-2 * t + 2) ^ 3 / 2
		return 1 - FMath::Pow(-2 * Time + 2, 3) / 2;
	}
}
#pragma endregion

#pragma region Ease Quint
float UABTweener_Ease::EaseInQuint(float Time)
{
	float QuinticTime = Time * Time * Time * Time * Time;
	//x*x*x*x*x
	return QuinticTime;
}

float UABTweener_Ease::EaseOutQuint(float Time)
{
	//1-(1-x)^5
	return 1 - FMath::Pow(1 - Time, 5);
}

float UABTweener_Ease::EaseInOutQuint(float Time)
{
	float QuinticTime = Time * Time * Time * Time * Time;
	if (Time < 0.5)
	{
		//16*x*x*x*x*x
		return 16 * QuinticTime;
	}
	else
	{
		//1 (-2 * t + 2) ^ 5 / 2
		return 1 - FMath::Pow(-2 * Time + 2, 5) / 2;
	}
}
#pragma endregion

#pragma region Ease Circ
float UABTweener_Ease::EaseInCirc(float Time)
{	//	  _______																	   _____
	//1-\/1 - x^2 (ma non so perch� nella grafica funziona senza la potenza, ossia 1-\/(1-x)
	return 1 - FMath::Sqrt(1 - FMath::Pow(Time, 2)); //????
}

float UABTweener_Ease::EaseOutCirc(float Time)
{
	//	_____
	//\/1-(x-1^2)
	return FMath::Sqrt(1 - FMath::Pow(Time - 1, 2)); //???
}

float UABTweener_Ease::EaseInOutCirc(float Time)
{
	if (Time < .5f)
	{
		//		 _______
		//(1 - \/1 - (2 * x^2))/2
		return(1 - FMath::Sqrt(1 - FMath::Pow(2 * Time, 2))) / 2;
	}
	else
	{
		//	 ________
		//(\/1-((-2 * x)+2)^2)+1)/2
		return (FMath::Sqrt(1 - FMath::Pow(-2 * Time + 2, 2)) + 1) / 2;
	}
	return 0.0f;
}
#pragma endregion

#pragma region Ease Elastic
float UABTweener_Ease::EaseInElastic(float Time)
{
	if (Time == 0)
	{
		return 0.0f;
	}
	else if (Time == 1)
	{
		return 1;
	}
	else
	{
		//(-2^10 * x - 10) * sin(x * 10 - 10.75) * 2 * 3,14 / 3
		return -(FMath::Pow(2, 10 * Time - 10)) * FMath::Sin((Time * 10 - 10.75) * ((2 * Pi) / 3));
	}
}

float UABTweener_Ease::EaseOutElastic(float Time)
{
	if (Time == 0)
	{
		return 0.0f;
	}
	else if (Time == 1)
	{
		return 1;
	}
	else
	{
		// 2 ^ 10 * x * sin((x * 10 - 0.75) * (2 * 3.14) / 3 +1 
		return FMath::Pow(2, -10 * Time) * FMath::Sin((Time * 10 - 0.75) * (2 * Pi) / 3) + 1;
	}
}

float UABTweener_Ease::EaseInOutElastic(float Time)
{
	if (Time == 0)
	{
		return 0.0f;
	}
	else if (Time == 1)
	{
		return 1;
	}
	else
	{
		if (Time < 0.5)
		{
			//(-2^20*x-10) * sin(20*x-11.125)*(2*3.14)/2
			return -(FMath::Pow(2, 20 * Time - 10)) * FMath::Sin((20 * Time - 11.125) * (2 * Pi)) / 2;
		}
		else
		{
			//(2^-20*x+10) * sin(20*x-11.125)*(2*3.14)/2+1
			return (FMath::Pow(2, -20 * Time + 10)) * FMath::Sin((20 * Time - 11.125) * (2 * Pi)) / 2 + 1;
		}
	}
}
#pragma endregion

#pragma region Ease Quad
float UABTweener_Ease::EaseInQuad(float Time)
{
	float DoubleTime = Time * Time;
	//x*x
	return DoubleTime;
}

float UABTweener_Ease::EaseOutQuad(float Time)
{
	float MinusOneTime = (1 - Time);
	//1-(1-x)*(1-x)
	return 1 - MinusOneTime * MinusOneTime;
}

float UABTweener_Ease::EaseInOutQuad(float Time)
{
	float DoubleTime = Time * Time;
	if (Time < .5f)
	{
		//2*x*x
		return 2 * DoubleTime;
	}
	else
	{
		//1-(-2*x+2)^2/2
		return 1 - FMath::Pow(-2 * Time + 2, 2) / 2;
	}
}
#pragma endregion

#pragma region Ease Quart
float UABTweener_Ease::EaseInQuart(float Time)
{
	float FourTimesTime = Time * Time * Time * Time;
	//x*x*x*x
	return FourTimesTime;
}

float UABTweener_Ease::EaseOutQuart(float Time)
{
	//1-(1-x)^4
	return 1 - FMath::Pow(1 - Time, 4);
}

float UABTweener_Ease::EaseInOutQuart(float Time)
{
	float FourTimesTime = Time * Time * Time * Time;
	if (Time < .5f)
	{
		//8*x*x*x*x
		return 8 * FourTimesTime;
	}
	else
	{
		//1-(-2*x+2)^4/2
		return 1 - FMath::Pow(-2 * Time + 2, 4) / 2;
	}
}
#pragma endregion

#pragma region Ease Expo
float UABTweener_Ease::EaseInExpo(float Time)
{
	if (Time == 0)
	{
		return 0.0f;
	}
	else
	{
		//2^10*x-10
		return FMath::Pow(2, 10 * Time - 10);
	}
}

float UABTweener_Ease::EaseOutExpo(float Time)
{
	if (Time == 1)
	{
		return 1.0f;
	}
	else
	{
		//1-2^-10*x
		return 1 - FMath::Pow(2, -10 * Time);
	}
}

float UABTweener_Ease::EaseInOutExpo(float Time)
{
	if (Time == 0)
	{
		return 0.0f;
	}
	if (Time == 1)
	{
		return 1.0f;
	}
	
		if (Time < .5f)
		{
			//2^20*x-10/2
			return FMath::Pow(2, 20 * Time - 10) / 2;
		}
		else
		{
			//2-2^-20*x+10/2
			return (2 - FMath::Pow(2, -20 * Time + 10))/ 2;
		}

}
#pragma endregion

#pragma region Ease Back
float UABTweener_Ease::EaseInBack(float Time)
{
	float Curve1 = 1.70;
	float Curve2 = Curve1 + 1;
	float CubicTime = Time * Time * Time;
	float DoubleTime = Time * Time;

	//((1.70+1)*x*x*x)-(1.70*x*x)
	return Curve2 * CubicTime - Curve1 * DoubleTime;
}

float UABTweener_Ease::EaseOutBack(float Time)
{
	float Curve1 = 1.70;
	float Curve2 = Curve1 + 1;

	//1+(1.70+1) * (x-1)^3 + 1.70 * (x-1)^2
	return 1 + Curve2 * FMath::Pow(Time - 1, 3) + Curve1 * FMath::Pow(Time - 1, 2);
}

float UABTweener_Ease::EaseInOutBack(float Time)
{
	float Curve = 1.70 * 1.50;

	if (Time < 0.5)
	{
		//((2*x)^2 * (((1.7*1.5)+1)) *2*x-(1.7*1.5))/2
		return (FMath::Pow(2 * Time, 2) * ((Curve + 1) * 2 * Time - Curve)) / 2;
	}
	else
	{
		//(2*x-2)^2+(((1.7*1.5)+1)*(x*2-2)+(1.7*1.5)+2)/2
		return(FMath::Pow(2 * Time - 2, 2) * ((Curve + 1) * (Time * 2 - 2) + Curve) + 2) / 2;
	}
}
#pragma endregion

#pragma region Ease Bounce
float UABTweener_Ease::EaseInBounce(float Time)
{
	return 1 - EaseOutBounce(1 - Time);
}

float UABTweener_Ease::EaseOutBounce(float Time)
{
	float DoubleTime = Time * Time;
	float Normal = 7.56;
	float Distance = 2.75;

	if (Time < 1 / Distance)
	{
		//7.56*x*x
		return Normal * DoubleTime;
	}
	else if (Time < 2 / Distance)
	{
		//7.56*(x-=1.5/2.75)*x+0.75
		return Normal * (Time -= 1.5 / Distance) * Time + .75f;
	}
	else if (Time < 2.5 / Distance)
	{
		//7.56*(x-=2.25/2.75)*x+0.94
		return Normal * (Time -= 2.25 / Distance) * Time + 0.94f;
	}
	else
	{
		//7.56*(x-=2.6/2.75)*x+0.98
		return Normal * (Time -= 2.6 / Distance) * Time + .98f;
	}

}

float UABTweener_Ease::EaseInOutBounce(float Time)
{
	if (Time < 0.5)
	{
		return (1 - EaseOutBounce(1 - 2 * Time)) / 2;
	}
	else
	{
		return(1 + EaseOutBounce(2 * Time - 1)) / 2;
	}
}
#pragma endregion
