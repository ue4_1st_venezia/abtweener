// Fill out your copyright notice in the Description page of Project Settings.


#include "ABTweener_BPFunctionLibrary.h"
#include "ABTweener_Module.h"

float UABTweener_BPFunctionLibrary::Ease(ETweenEase EaseType, float t)
{
	return UABTweener_Ease::Ease(EaseType, t);
}

void UABTweener_BPFunctionLibrary::EnsureCapacityTween(int NumVector2D)
{
	UABTweener_Module::EnsureCapacity(NumVector2D);
}
