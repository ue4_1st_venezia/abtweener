// Fill out your copyright notice in the Description page of Project Settings.


#include "ABTweener_BPAsyncActionFloat.h"
#include "ABTweener_Module.h"

UABTweener_BPAsyncActionFloat* UABTweener_BPAsyncActionFloat::TweenFloat(float Start, float End, float DurationSecs, ETweenEase EaseType, float Delay, int Loops, float LoopDelay, bool bYoyo, float YoyoDelay, bool bCanTickDuringPause, bool bCanUseGlobalTimeDilatation)
{
	UABTweener_BPAsyncActionFloat* BlueprintNode = NewObject<UABTweener_BPAsyncActionFloat>();
	BlueprintNode->SetSharerdTweenProperty(DurationSecs, Delay, Loops, LoopDelay, bYoyo, YoyoDelay, bCanTickDuringPause, bCanUseGlobalTimeDilatation);
	BlueprintNode->BP_EaseType = EaseType;
	BlueprintNode->Start = Start;
	BlueprintNode->End = End;
	return BlueprintNode;
}

UABTweener_Instance* UABTweener_BPAsyncActionFloat::CreateTween()
{
	//Creiamo il tween
	return UABTweener_Module::Play(Start, End, [&](float t) {ApplyEasing.Broadcast(t); }, BP_DurationSecs, BP_EaseType);
}
