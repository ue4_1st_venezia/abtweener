// Copyright Epic Games, Inc. All Rights Reserved.

#include "ABTween.h"
#include "ABTweener_Module.h"

#define LOCTEXT_NAMESPACE "FABTweenModule"

// In questa classe non facciamo nulla se non prendere le reference alle funzioni scritte nella classe UABTweener_Module
void FABTweenModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	UABTweener_Module::Inizialize();
}

void FABTweenModule::ShutdownModule()
{
	UABTweener_Module::Deinizialize();
}
#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FABTweenModule, ABTween)
