// Fill out your copyright notice in the Description page of Project Settings.


#include "ABTweener_BPAsyncActionBase.h"
#include "ABTweener_Module.h"

void UABTweener_BPAsyncActionBase::Activate()
{
	//Per prima cosa si controlla se la nostra istanza sia diversa da nullptr
	if (TweenerInstance != nullptr)
	{
		//se lo � la distuggiamo e la rendiamo nulla per non incorrere in problemi dopo
		TweenerInstance->Destroy();
		TweenerInstance = nullptr;
	}

	//Poi controlliamo se la durata di un tween � minore o uguale a 0. Se lo � si ferma il tutto
	if (BP_DurationSecs <= 0) return;

	//In seguito creaimo un nuovo Tween
	TweenerInstance = CreateTween();

	//Dopo aver creato il tween si controlla se � uguale a nullptr. Se lo � si ferma il tutto.
	if (TweenerInstance == nullptr) return;

	//Dopodiche settiamo tutti i valori per il blueprint
	TweenerInstance->SetDelay(BP_Delay);
	TweenerInstance->SetLoops(BP_Loops);
	TweenerInstance->SetLoopDelay(BP_LoopDelay);
	TweenerInstance->SetYoyo(BP_bYoyo);
	TweenerInstance->SetYoyoDelay(BP_YoyoDelay);
	TweenerInstance->SetCanTickDuringPause(BP_bCanTickDuringPause);
	TweenerInstance->SetGlobalTimeDilatation(BP_bUseGlobalTimeDilatation);
	TweenerInstance->SetAutoDestroy(false);

	if (OnLoop.IsBound())
		TweenerInstance->SetOnLoop([&]() {OnLoop.Broadcast(); });
	if (OnYoyo.IsBound())
		TweenerInstance->SetOnYoyo([&]() {OnYoyo.Broadcast(); });
	if (OnComplete.IsBound())
		TweenerInstance->SetOnCompleted([&]() {OnComplete.Broadcast(); Stop(); });

}

UABTweener_Instance* UABTweener_BPAsyncActionBase::CreateTween()
{
	return nullptr;
}

void UABTweener_BPAsyncActionBase::SetSharerdTweenProperty(float InDurationSecs, float InDelay, int InLoops, float InLoopDelay, bool bInYoyo, float InYoyoDelay, bool bInCanTickDuringPause, bool bInCanUseGlobalTimeDilatation)
{
	//Qui settiamo tutti i valori di inizializzazione che saranno messi da blueprint
	TweenerInstance = nullptr;
	BP_DurationSecs = InDurationSecs;
	BP_Delay = InDelay;
	BP_Loops = InLoops;
	BP_LoopDelay = InLoopDelay;
	BP_bYoyo = bInYoyo;
	BP_YoyoDelay = InYoyoDelay;
	BP_bCanTickDuringPause = bInCanTickDuringPause;
	BP_bUseGlobalTimeDilatation = bInCanUseGlobalTimeDilatation;
}

void UABTweener_BPAsyncActionBase::BeginDestroy()
{
	//Qui distruggiamo la nostra istanza
	Super::BeginDestroy();
	//Per prima cosa si controlla se la nostra istanza � diversa da nullptr
	if (TweenerInstance != nullptr)
	{
		//se lo � la distuggiamo e la rendiamo nulla per non incorrere in problemi dopo
		TweenerInstance->Destroy();
		TweenerInstance = nullptr;
	}
}

void UABTweener_BPAsyncActionBase::Pause()
{
	//Qui pausiamo il tween
	if (TweenerInstance) TweenerInstance->Pause();
}

void UABTweener_BPAsyncActionBase::Unpause()
{
	//Qui togliamo la pausa al tween
	if (TweenerInstance) TweenerInstance->Unpause();
}

void UABTweener_BPAsyncActionBase::Restart()
{
	//Qui facciamo ripartire il tween
	if (TweenerInstance) TweenerInstance->Restart();
}

void UABTweener_BPAsyncActionBase::Stop()
{
	//Qui stoppiamo il tween
	if (TweenerInstance)
	{
		//Quindi per prima cosa lo ditruggiamo e lo rendiamo nullptr
		TweenerInstance->Destroy();
		TweenerInstance = nullptr;
		//Poi lo rendiamo pronto per essere distrutto per l'engine
		SetReadyToDestroy();
		//Quindi lo marchiamo come spazzatura
		MarkAsGarbage();
	}
}

void UABTweener_BPAsyncActionBase::SetTimeMultiplier(float Multiplier)
{
	//Qui gli settiamo il TimeMultiplier
	if (TweenerInstance) TweenerInstance->SetTimeMultiplier(Multiplier);
}
