// Fill out your copyright notice in the Description page of Project Settings.


#include "ABTweener_BPAsyncActionVector2D.h"
#include "ABTweener_Module.h"

UABTweener_BPAsyncActionVector2D* UABTweener_BPAsyncActionVector2D::TweenVector2D(FVector2D Start, FVector2D End, float DurationSecs,
	ETweenEase EaseType, float Delay, int Loops, float LoopDelay, bool bYoyo, float YoyoDelay, bool bCanTickDuringPause, bool bCanUseGlobalTimeDilatation)
{
	//Inizializzamo tutti i valori che poi verranno settati dentro il nodo del Blueprint
	UABTweener_BPAsyncActionVector2D* BlueprintNode = NewObject<UABTweener_BPAsyncActionVector2D>();
	BlueprintNode->SetSharerdTweenProperty(DurationSecs, Delay, Loops, LoopDelay, bYoyo, YoyoDelay, bCanTickDuringPause, bCanUseGlobalTimeDilatation);
	BlueprintNode->BP_EaseType = EaseType;
	BlueprintNode->Start = Start;
	BlueprintNode->End = End;
	return BlueprintNode;
}

UABTweener_Instance* UABTweener_BPAsyncActionVector2D::CreateTween()
{
	//Creiamo il tween
	return UABTweener_Module::Play(Start, End, [&](FVector2D t) {ApplyEasing.Broadcast(t); }, BP_DurationSecs, BP_EaseType);
}
