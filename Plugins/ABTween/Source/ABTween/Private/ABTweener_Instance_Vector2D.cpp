// Fill out your copyright notice in the Description page of Project Settings.


#include "ABTweener_Instance_Vector2D.h"

void UABTweener_Instance_Vector2D::Initialize(FVector2D InStart, FVector2D InEnd, TFunction<void(FVector2D)> InOnUpdate, float InDurationSecs, ETweenEase InEaseType)
{
	//Inizializzamo i valori
	this->StartValue = InStart;
	this->EndValue = InEnd;
	this->OnUpdate = MoveTemp(InOnUpdate);
	this->InitializeSharedMembers(InDurationSecs, InEaseType);
}

void UABTweener_Instance_Vector2D::ApplyEasing(float EasedPercent)
{
	OnUpdate(FMath::Lerp<FVector2D>(StartValue, EndValue, EasedPercent));
}