// Fill out your copyright notice in the Description page of Project Settings.

#include "ABTweener_Module.h"
#include "ABTweener_Manager.h"

//Creiamo delle situazioni per il default cos� da non distruggere tutto(?)
 //Una capienza di default per i tween
const int DEFAULT_VECTOR2D_TWEEN_CAPACITY = 50;
const int DEFAULT_FLOAT_TWEEN_CAPACITY = 50;
//Rendiamo nullptr la referenza T del Manager del Tween2D
UABTweener_Manager<UABTweener_Instance_Vector2D>* UABTweener_Module::Vector2DTweenerManager = nullptr; 
UABTweener_Manager<UABTweener_Instance_Float>* UABTweener_Module::FloatTweenerManager = nullptr; 
// Assegnamo il defautl al numero dei Vettori 2d riservati
int UABTweener_Module::NumReservedVector2D = DEFAULT_VECTOR2D_TWEEN_CAPACITY;
int UABTweener_Module::NumReservedFloat = DEFAULT_FLOAT_TWEEN_CAPACITY;

void UABTweener_Module::Inizialize()
{
	//All'inizializzazione creiamo un nuovo manager per le istanze dei Vector2D assegnandogli il defaut creato prima.
	Vector2DTweenerManager = new UABTweener_Manager<UABTweener_Instance_Vector2D>(DEFAULT_VECTOR2D_TWEEN_CAPACITY);
	FloatTweenerManager = new UABTweener_Manager<UABTweener_Instance_Float>(DEFAULT_FLOAT_TWEEN_CAPACITY);
	//Poi assegnamo il default al numero dei Vettori 2d riservati
	NumReservedVector2D = DEFAULT_VECTOR2D_TWEEN_CAPACITY;
	NumReservedFloat = DEFAULT_FLOAT_TWEEN_CAPACITY;
}

void UABTweener_Module::Deinizialize()
{
	//Nella Deinizializzazione cancelliamo tutto il nostro manager
	delete Vector2DTweenerManager;
	delete FloatTweenerManager;
}

void UABTweener_Module::EnsureCapacity(int NumVector2DTweens, int NumFloatTweens)
{
	//Qui garantiamo la capacit� dei tween dentro il manager.
	Vector2DTweenerManager->EnsureCapacity(NumVector2DTweens);
	FloatTweenerManager->EnsureCapacity(NumFloatTweens);

	NumReservedVector2D = Vector2DTweenerManager->GetCurrentCapacity();
	NumReservedFloat = FloatTweenerManager->GetCurrentCapacity();
}

void UABTweener_Module::EnsureCapacity(int NumTweens)
{
	EnsureCapacity(NumTweens, NumTweens);
}

void UABTweener_Module::Update(float UnscaledDeltaSeconds, float DilatatedDeltaSeconds, bool bIsGamePaused)
{
	//Qui aggiorniamo il manager dei tween 2D.
	Vector2DTweenerManager->Update(UnscaledDeltaSeconds, DilatatedDeltaSeconds, bIsGamePaused);
	FloatTweenerManager->Update(UnscaledDeltaSeconds, DilatatedDeltaSeconds, bIsGamePaused);
}

void UABTweener_Module::ClearActiveTweens()
{
	//Qui puliamo il manager dei tween 2D.
	Vector2DTweenerManager->ClearActiveTweens();
	FloatTweenerManager->ClearActiveTweens();
}

int UABTweener_Module::CheckTweenCapacity()
{
	//Controlliamo se la nostra capacit� � maggiore del numero dei tween riservati 
	if (Vector2DTweenerManager->GetCurrentCapacity() > NumReservedVector2D)
	{
		//Se lo � avvisiamo l'utente
		UE_LOG(LogTemp, Warning, 
		TEXT("Consider increasing initial capacity for Vector2D tweens with UABTweener_Manager::EnsureCapacity().%d were initially reserved, but now there are %d in memory.")
			, NumReservedVector2D, Vector2DTweenerManager->GetCurrentCapacity());
	}

	if (FloatTweenerManager->GetCurrentCapacity() > NumReservedFloat)
	{
		//Se lo � avvisiamo l'utente
		UE_LOG(LogTemp, Warning,
			TEXT("Consider increasing initial capacity for Float tweens with UABTweener_Manager::EnsureCapacity().%d were initially reserved, but now there are %d in memory.")
			, NumReservedFloat, FloatTweenerManager->GetCurrentCapacity());
	}

	//altrimenti ritorniamo la CurrentCapacity
	return Vector2DTweenerManager->GetCurrentCapacity() + FloatTweenerManager->GetCurrentCapacity();
}

float UABTweener_Module::Ease(float t, ETweenEase EaseType)
{
	//Qui ritorniamo il tipo di Ease da utilizzare
	return UABTweener_Ease::Ease(EaseType, t);
}

UABTweener_Instance_Vector2D* UABTweener_Module::Play(FVector2D Start, FVector2D End, TFunction<void(FVector2D)> OnUpdate, float DurationSecs, ETweenEase EaseType)
{
	//Qui per prima cosa creaimo un nuovo Tween
	UABTweener_Instance_Vector2D* NewTween = Vector2DTweenerManager->CreateTween();
	//Lo Inizializzamo con i valori in imput
	NewTween->Initialize(Start, End, MoveTemp(OnUpdate), DurationSecs, EaseType);
	//E infine lo ritorniamo
	return NewTween;
}

UABTweener_Instance_Float* UABTweener_Module::Play(float Start, float End, TFunction<void(float)> OnUpdate, float DurationSecs, ETweenEase EaseType)
{
	//Qui per prima cosa creaimo un nuovo Tween
	UABTweener_Instance_Float* NewTween = FloatTweenerManager->CreateTween();
	//Lo Inizializzamo con i valori in imput
	NewTween->Initialize(Start, End, MoveTemp(OnUpdate), DurationSecs, EaseType);
	//E infine lo ritorniamo
	return NewTween;
}
