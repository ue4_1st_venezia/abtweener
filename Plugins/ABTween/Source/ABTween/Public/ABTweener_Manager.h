// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

template <class T>
class ABTWEEN_API UABTweener_Manager
{
	typedef typename TDoubleLinkedList<T*>::TDoubleLinkedListNode TNode;

private:
	TDoubleLinkedList<T*>* ActiveTweens;		//I Tween Attivi
	TDoubleLinkedList<T*>* RecycledTweens;		//I Tween Riciclati
	TDoubleLinkedList<T*>* TweensToActivate;	//I tween da attivare al prossimo update

public:
	//Costruttore. 
	UABTweener_Manager(int Capacity)
	{
		//Qui creiamo i nuovi oggetti
		ActiveTweens = new TDoubleLinkedList<T*>();
		RecycledTweens = new TDoubleLinkedList<T*>();
		TweensToActivate = new TDoubleLinkedList<T*>();

		//aggiungiamo alla coda i tween riciclati
		for (int i = 0; i < Capacity; ++i)
		{
			RecycledTweens->AddTail(new T);
		}
	}

	//Distruttore
	~UABTweener_Manager()
	{
		//Per prima cosa settiamo il CurrNode uguale al RicycledTween che sta in testa alla lista
		TNode* CurrNode = RecycledTweens->GetHead();
		//facciamo un ciclo e controlliamo se il Nodo non � nullptr
		while (CurrNode != nullptr)
		{
			//Se non lo � lo deletiamo e andiamo avanti
			delete CurrNode->GetValue();
			CurrNode = CurrNode->GetNextNode();
		}
		//Deletiamo il RecycleTween
		delete RecycledTweens;

		//Poi settiamo il CurrNode uguale al ActiveTween che sta in testa alla lista
		CurrNode = ActiveTweens->GetHead();
		//facciamo un ciclo e controlliamo se il Nodo non � nullptr
		while (CurrNode != nullptr)
		{
			//Se non lo � lo deletiamo e andiamo avanti
			delete CurrNode->GetValue();
			CurrNode = CurrNode->GetNextNode();
		}
		//Deletiamo il ActiveTween
		delete ActiveTweens;

		//Poi settiamo il CurrNode uguale al TweenToActivate che sta in testa alla lista
		CurrNode = TweensToActivate->GetHead();
		//facciamo un ciclo e controlliamo se il Nodo non � nullptr
		while (CurrNode != nullptr)
		{
			//Se non lo � lo deletiamo e andiamo avanti
			delete CurrNode->GetValue();
			CurrNode = CurrNode->GetNextNode();
		}
		//Deletiamo il TweenToActivate
		delete TweensToActivate;
	}

	//Garaniamo la capacit� dei tween
	void EnsureCapacity(int Num)
	{
		//Creiamo un intero che sar� uguale alla somma dei Tween attivi con i Tween da attivare e i Tween riciclati
		int NumExistingTween = ActiveTweens->Num() + TweensToActivate->Num() + RecycledTweens->Num();
		//Poi facciamo un ciclo che parte dal nostro intero e che deve essere minore dell'intero preso in input, che poi verr� aggiunto in coda
		for (int i = NumExistingTween; i < Num; ++i)
		{
			//Aggiungiamo alla coda dei tween riciclati un nuovo elemento T
			RecycledTweens->AddTail(new T());
		}
	}

	//Otteniamo la capacit� corrente
	int GetCurrentCapacity()
	{
		//questo ritorna la somma dei tween attivi con i tween da attivare e i tween riciclati
		return ActiveTweens->Num() + TweensToActivate->Num() + RecycledTweens->Num();
	}

	//Aggiornamento
	void Update(float UnscaledDeltaSeconds, float DilatedDeltaSeconds, bool bIsPaused)
	{
		//Per prima cosa aggiungiamo tween in sospeso creando un nuovo nodo che sar� uguale al tween alla testa dei tween da attivare
		TNode* CurrNode = TweensToActivate->GetHead();
		//Poi facciamo un ciclo controllando che il nodo non sia nullptr
		while (CurrNode != nullptr)
		{
			//Creiamo un nuovo nodo che sar� uguale al nodo creato sopra
			TNode* NodeToActivate = CurrNode;
			//Assegnamo il vecchio nodo uguale al nodo successivo.
			CurrNode = CurrNode->GetNextNode();
			//Facciamo partire il nodo da attivare
			NodeToActivate->GetValue()->Start();
			//Rimuoviamo il nodo dai tween da attivare, senza per� cancellarlo.
			TweensToActivate->RemoveNode(NodeToActivate, false);
			//E infine aggiungiamo alla coda dei Tween attivi il nodo da attivare
			ActiveTweens->AddTail(NodeToActivate);
		}

		//Adesso aggiorniamo i tween usando sempre il nodo sopra creato che sar� uguale al tween alla testa dei tween attivi
		CurrNode = ActiveTweens->GetHead();
		//Poi facciamo un ciclo controllando che il nodo non sia nullptr
		while (CurrNode != nullptr)
		{
			//Ora creiamo una referenza alla nostra istanza e la castiamo come static_cast
			UABTweener_Instance* CurrTween = static_cast<UABTweener_Instance*>(CurrNode->GetValue());
			//Aggiorniamo il nostro CurrTween con la sua funzione
			CurrTween->Update(UnscaledDeltaSeconds, DilatedDeltaSeconds, bIsPaused);
			//Assegnamo il vecchio nodo uguale al nodo successivo.
			TNode* NextNode = CurrNode->GetNextNode();
			//Controlliamo se la nostra istanza non � attiva
			if (!CurrTween->bIsActive)
			{
				//se non � attiva rimuoviamo il nodo senza distruggerlo
				ActiveTweens->RemoveNode(CurrNode, false);
				//e lo aggiungiamo ai tween da riciclare
				RecycledTween(CurrNode);
			}
			//altrimenti assegnamo il CurrNode come il NextNode
			CurrNode = NextNode;

		}
	}

	//Pulizia
	void ClearActiveTweens()
	{
		//Per prima cosa aggiungiamo tween in sospeso creando un nuovo nodo che sar� uguale al tween alla testa dei tween da attivare
		TNode* CurrNode = TweensToActivate->GetHead();
		//Poi facciamo un ciclo controllando che il nodo non sia nullptr
		while (CurrNode != nullptr)
		{
			//Creiamo un nuovo nodo che sar� uguale al nodo creato sopra
			TNode* NodeToRecycle = CurrNode;
			//Assegnamo il vecchio nodo uguale al nodo successivo.
			CurrNode = CurrNode->GetNextNode();

			//Distruggiamo il nuovo nodo
			NodeToRecycle->GetValue()->Destroy();
			//Rimuoviamo il nodo dai tween da riciclare, senza per� cancellarlo.
			TweensToActivate->RemoveNode(NodeToRecycle, false);
			//E infine aggiungiamo alla coda dei Tween da riciclare il nodo da riciclare
			RecycledTweens->AddTail(NodeToRecycle);
		}

		//Poi settiamo il CurrNode uguale al ActiveTween che sta in testa alla lista
		CurrNode = ActiveTweens->GetHead();
		//facciamo un ciclo e controlliamo se il Nodo non � nullptr
		while (CurrNode != nullptr)
		{
			//Creiamo un nuovo nodo che sar� uguale al nodo creato sopra
			TNode* NodeToRecycle = CurrNode;
			//Assegnamo il vecchio nodo uguale al nodo successivo.
			CurrNode = CurrNode->GetNextNode();
			//Rimuoviamo i Tween Attivi
			ActiveTweens->RemoveNode(NodeToRecycle, false);
			//E infine aggiungiamo alla coda dei Tween da riciclare il nodo da riciclare
			RecycledTweens->AddTail(NodeToRecycle);
		}
	}

	//Creazione dei Tween
	T* CreateTween()
	{
		//Creiamo un nuovo TNode.
		TNode* NewTween = GetNewTween();
		//Lo aggiungiamo alla cosa dei nodi da attivare.
		TweensToActivate->AddTail(NewTween);
		//E ritorniamo il valore del nuovo tween
		return NewTween->GetValue();
	}

private:
	//Prendiamo un Nuovo Tween
	TNode* GetNewTween()
	{
		//Per prima cosa aggiungiamo tween in sospeso creando un nuovo nodo che sar� uguale al tween alla testa dei tween riciclati.
		TNode* Node = RecycledTweens->GetHead();
		//Poi controlliamo se questo nodo non � nullptr
		if (Node != nullptr)
		{
			//se non lo � rimuoviamo il nodo dai tween da riciclare, senza cancellarlo
			RecycledTweens->RemoveNode(Node, false);
			//e ritorniamo il Nodo
			return Node;
		}
		//altrimenti se il nodo � nullptr ritorniamo un nuovo nodo
		return new TNode(new T());
	}

	//Riciclo dei tween
	void RecycledTween(TNode* ToRecycle)
	{
		//Aggiumgiamo alla coda dei tween riciclati il nodo in input
		RecycledTweens->AddTail(ToRecycle);
	}
};
