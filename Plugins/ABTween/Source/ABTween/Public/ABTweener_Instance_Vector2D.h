// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ABTweener_Instance.h"

/**
 * 
 */
class ABTWEEN_API UABTweener_Instance_Vector2D : public UABTweener_Instance
{
public:
	FVector2D StartValue;
	FVector2D EndValue;
	TFunction<void(FVector2D)> OnUpdate;

	void Initialize(FVector2D InStart, FVector2D InEnd, TFunction<void(FVector2D)> InOnUpdate, float InDurationSecs, ETweenEase InEaseType);

protected:
	virtual void ApplyEasing(float EasedPercent) override;
};
