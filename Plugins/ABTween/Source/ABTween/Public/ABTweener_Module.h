// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ABTweener_Ease.h"
#include "ABTweener_Instance.h"
#include "ABTweener_Instance_Vector2D.h"
#include "ABTweener_Instance_Float.h"
#include "ABTweener_Manager.h"
/**
 * 
 */
class ABTWEEN_API UABTweener_Module : public IModuleInterface
{
private:
	static UABTweener_Manager<UABTweener_Instance_Vector2D>* Vector2DTweenerManager;
	static int NumReservedVector2D;

	static UABTweener_Manager<UABTweener_Instance_Float>* FloatTweenerManager;
	static int NumReservedFloat;

public:
	static void Inizialize();
	static void Deinizialize();

	//Questo � il primo EnsureCapacity
	static void EnsureCapacity(int NumVector2DTweens, int NumFloatTweens);
	//Questo � il secondo EnsureCapacity
	static void EnsureCapacity(int NumTweens);
	static void Update(float UnscaledDeltaSeconds, float DilatatedDeltaSeconds, bool bIsGamePaused);
	static void ClearActiveTweens();
	static int CheckTweenCapacity();
	static float Ease(float t, ETweenEase EaseType);
	static UABTweener_Instance_Vector2D* Play(FVector2D Start, FVector2D End, TFunction<void(FVector2D)> OnUpdate, float DurationSecs, ETweenEase EaseType = ETweenEase::None);
	static UABTweener_Instance_Float* Play(float Start, float End, TFunction<void(float)> OnUpdate, float DurationSecs, ETweenEase EaseType = ETweenEase::None);
};
