// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "CoreMinimal.h"
#include "ABTweener_Instance.h"
#include "ABTweener_Object.generated.h"

/**
 * 
 */
UCLASS()
class ABTWEEN_API UABTweener_Object : public UObject
{
	GENERATED_BODY()
	
public:
	UABTweener_Object();
	UABTweener_Instance* Tween;
	virtual void BeginDestroy() override;

	void SetTweenerInstance(UABTweener_Instance* InTween);
	void Destroy();
};
