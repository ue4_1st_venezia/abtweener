// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#pragma region Ease Enum
//Qui c'� da creare l'enumerato delle varie ease da richiamare poi nella classe effettiva
UENUM(BlueprintType)
enum class ETweenEase : uint8
{
	None,
	EaseLinear,
	EaseSmoothstep,
	EaseInSin,
	EaseOutSine,
	EaseInOutSine,
	EaseInCubic,
	EaseOutCubic,
	EaseInOutCubic,
	EaseInQuint,
	EaseOutQuint,
	EaseInOutQuint,
	EaseInCirc,
	EaseOutCirc,
	EaseInOutCirc,
	EaseInElastic,
	EaseOutElastic,
	EaseInOutElastic,
	EaseInQuad,
	EaseOutQuad,
	EaseInOutQuad,
	EaseInQuart,
	EaseOutQuart,
	EaseInOutQuart,
	EaseInExpo,
	EaseOutExpo,
	EaseInOutExpo,
	EaseInBack,
	EaseOutBack,
	EaseInOutBack,
	EaseInBounce,
	EaseOutBounce,
	EaseInOutBounce
};
#pragma endregion
/**
 *
 */
const float Pi = 3.14159265f;

class ABTWEEN_API UABTweener_Ease 
{
public:

	static float Ease(ETweenEase EaseType, float Time);

#pragma region All Ease Functions
	static float EaseLinear(float Time);
	static float EaseSmoothstep(float Time, float Param1, float Param2);
	static float EaseInSin(float Time);
	static float EaseOutSine(float Time);
	static float EaseInOutSine(float Time);
	static float EaseInCubic(float Time);
	static float EaseOutCubic(float Time);
	static float EaseInOutCubic(float Time);
	static float EaseInQuint(float Time);
	static float EaseOutQuint(float Time);
	static float EaseInOutQuint(float Time);
	static float EaseInCirc(float Time);
	static float EaseOutCirc(float Time);
	static float EaseInOutCirc(float Time);
	static float EaseInElastic(float Time);
	static float EaseOutElastic(float Time);
	static float EaseInOutElastic(float Time);
	static float EaseInQuad(float Time);
	static float EaseOutQuad(float Time);
	static float EaseInOutQuad(float Time);
	static float EaseInQuart(float Time);
	static float EaseOutQuart(float Time);
	static float EaseInOutQuart(float Time);
	static float EaseInExpo(float Time);
	static float EaseOutExpo(float Time);
	static float EaseInOutExpo(float Time);
	static float EaseInBack(float Time);
	static float EaseOutBack(float Time);
	static float EaseInOutBack(float Time);
	static float EaseInBounce(float Time);
	static float EaseOutBounce(float Time);
	static float EaseInOutBounce(float Time);
#pragma endregion	
};

