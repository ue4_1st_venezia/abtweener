// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ABTweener_Instance.h"

/**
 * 
 */

class ABTWEEN_API UABTweener_Instance_Float : public UABTweener_Instance
{
public:
	float StartValue;
	float EndValue;
	TFunction<void(float)> OnUpdate;

	void Initialize(float InStart, float InEnd, TFunction<void(float)> InOnUpdate, float InDurationSecs, ETweenEase InEaseType);

protected:
	virtual void ApplyEasing(float EasedPercent) override;
	
};
