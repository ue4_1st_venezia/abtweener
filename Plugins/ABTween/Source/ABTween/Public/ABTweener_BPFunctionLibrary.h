// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ABTweener_Ease.h"
#include "ABTweener_Module.h"
#include "ABTweener_BPFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class ABTWEEN_API UABTweener_BPFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintPure, Category = "Tween")
		static float Ease(ETweenEase EaseType, float t);
	UFUNCTION(BlueprintCallable, Category = "Tween|Utility")
		static void EnsureCapacityTween(int NumVector2D = 50);
};
