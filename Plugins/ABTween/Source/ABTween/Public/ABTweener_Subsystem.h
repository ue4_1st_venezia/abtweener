// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "CoreMinimal.h"
//#include "Subsystems/GameInstanceSubsystem.h"
#include "ABTweener_Subsystem.generated.h"

/**
 * 
 */
UCLASS()
class ABTWEEN_API UABTweener_Subsystem : public UGameInstanceSubsystem, public FTickableGameObject
{
	GENERATED_BODY()
	
private:
	UPROPERTY()
		uint32 LastTickedFrame;
	UPROPERTY()
		uint32 LastRealTimeSeconds;

public:
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;

	// Inherited via FTickableGameObject
	virtual void Tick(float DeltaTime) override;
	virtual ETickableTickType GetTickableTickType() const override;
	virtual TStatId GetStatId() const override;
	virtual bool IsTickableWhenPaused() const override;
	virtual bool IsTickableInEditor() const override;

};
