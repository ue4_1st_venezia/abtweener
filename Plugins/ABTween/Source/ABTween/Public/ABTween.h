// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once
#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

//Dato che si tratta di un plugin per modificare i comportamenti dei widget prendiamo le referenze da una classe Tickable
class FABTweenModule : public IModuleInterface
{
public:
	 /** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

};
