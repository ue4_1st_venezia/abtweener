// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ABTweener_Ease.h"

UENUM()
enum class EDelayState : uint8
{
	None,
	Start,
	Loop,
	Yoyo
};

/**
 * 
 */

class ABTWEEN_API UABTweener_Instance
{
public:
	float DurationInSecs;
	ETweenEase EaseType;
	float Counter;
	float DelayCounter;

	uint8 bShouldAutoDestroy		:1;	
	uint8 bIsActive					:1;	
	uint8 bIsPaused					:1;	
	uint8 bShouldYoyo				:1;	
	uint8 bIsPlayingYoyo			:1;	
	uint8 bCanTickDuringPause		:1;	
	uint8 bUseGlobalTimeDilatation	:1;	

	int NumLoops;
	int NumLoopCompleted;
	float DelaySecs;
	float LoopDelaySecs;
	float YoyoDelaySecs;
	float TimeMultiplier;

	EDelayState DelayState;

private:
	TFunction<void()> OnYoyo;
	TFunction<void()> OnLoop;
	TFunction<void()> OnCompleted;

public:
	UABTweener_Instance() {}
	virtual ~UABTweener_Instance() {}

	UABTweener_Instance* SetDelay(float InDelaySecs);
	UABTweener_Instance* SetLoops(int InNumLoops);
	UABTweener_Instance* SetLoopDelay(float InLoopDelaySecs);
	UABTweener_Instance* SetYoyo(bool bInShouldYoyo);
	UABTweener_Instance* SetYoyoDelay(float InYoyoDelaySecs);
	UABTweener_Instance* SetTimeMultiplier(float InTimeMultiplier);
	UABTweener_Instance* SetCanTickDuringPause(bool bInCanTickDuringPause);
	UABTweener_Instance* SetGlobalTimeDilatation(bool bInUseGlobalTimeDilatation);
	UABTweener_Instance* SetAutoDestroy(bool bInShouldAutoDestroy);

	UABTweener_Instance* SetOnYoyo(TFunction<void()> Handler);
	UABTweener_Instance* SetOnLoop(TFunction<void()> Handler);
	UABTweener_Instance* SetOnCompleted(TFunction<void()> Handler);

	void InitializeSharedMembers(float InDurationInSecs, ETweenEase InEaseType);
	void Start();
	void Restart();
	void Destroy();
	class UABTweener_Object* CreateObject(UObject* Outer = (UObject*)GetTransientPackage());
	void Pause();
	void Unpause();
	void Update(float UnscaleDeltaSeconds, float DilatedDeltaSeconds, bool bIsGamePaused = false);

protected:
	virtual void ApplyEasing(float EasedPercent) = 0;

private:
	void CompleteLoop();
	void StartNewLoop();
	void StartYoyo();
};