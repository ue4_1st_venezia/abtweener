// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ABTweener_BPAsyncActionBase.h"
#include "ABTweener_Module.h"
#include "ABTweener_Instance.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "ABTweener_BPAsyncActionVector2D.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTweenerUpdateVector2DOutputPin, FVector2D, Value);

UCLASS()
class ABTWEEN_API UABTweener_BPAsyncActionVector2D : public UABTweener_BPAsyncActionBase
{
	GENERATED_BODY()

public:
	FVector2D Start;
	FVector2D End;

	UPROPERTY(BlueprintAssignable)
		FTweenerUpdateVector2DOutputPin ApplyEasing;

	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", AdvancedDisplay = "4"), Category = "Tween")
		static UABTweener_BPAsyncActionVector2D* TweenVector2D(FVector2D Start = FVector2D::ZeroVector, FVector2D End = FVector2D::ZeroVector, float DurationSecs = 1.f,
															   ETweenEase EaseType = ETweenEase::None, float Delay = 0, int Loops = 0, float LoopDelay = 0, bool bYoyo = false,
															   float YoyoDelay = 0, bool bCanTickDuringPause = false, bool bCanUseGlobalTimeDilatation = true);

	virtual UABTweener_Instance* CreateTween() override;

	
};
