// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ABTweener_BPAsyncActionBase.h"
#include "ABTweener_Module.h"
#include "ABTweener_Instance.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "ABTweener_BPAsyncActionFloat.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTweenerUpdateFloatOutputPin, float, Value);

UCLASS()
class ABTWEEN_API UABTweener_BPAsyncActionFloat : public UABTweener_BPAsyncActionBase
{
	GENERATED_BODY()
	
public:
	float Start;
	float End;

	UPROPERTY(BlueprintAssignable)
		FTweenerUpdateFloatOutputPin ApplyEasing;

	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", AdvancedDisplay = "4"), Category = "Tween")
		static UABTweener_BPAsyncActionFloat* TweenFloat(float Start = 0, float End = 0, float DurationSecs = 1.f,
			ETweenEase EaseType = ETweenEase::None, float Delay = 0, int Loops = 0, float LoopDelay = 0, bool bYoyo = false,
			float YoyoDelay = 0, bool bCanTickDuringPause = false, bool bCanUseGlobalTimeDilatation = true);

	virtual UABTweener_Instance* CreateTween() override;
};
