// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ABTweener_Instance.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "ABTweener_BPAsyncActionBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTweenerEventOutputPin);

UCLASS(Abstract, BlueprintType, meta = (ExposedAsyncProxy = AsyncTask))
class ABTWEEN_API UABTweener_BPAsyncActionBase : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()

public:
	float BP_DurationSecs;
	ETweenEase BP_EaseType;
	float BP_Delay;
	int BP_Loops;
	float BP_LoopDelay;
	bool BP_bYoyo;
	float BP_YoyoDelay;
	bool BP_bCanTickDuringPause;
	bool BP_bUseGlobalTimeDilatation;

	class UABTweener_Instance* TweenerInstance = nullptr;

	UPROPERTY(BlueprintAssignable, AdvancedDisplay)
		FTweenerEventOutputPin OnLoop;
	UPROPERTY(BlueprintAssignable, AdvancedDisplay)
		FTweenerEventOutputPin OnYoyo;
	UPROPERTY(BlueprintAssignable, AdvancedDisplay)
		FTweenerEventOutputPin OnComplete;

	virtual void Activate() override;
	virtual UABTweener_Instance* CreateTween();
	virtual void SetSharerdTweenProperty(float InDurationSecs, float InDelay, int InLoops, float InLoopDelay, bool bInYoyo, float InYoyoDelay, bool bInCanTickDuringPause, bool bInCanUseGlobalTimeDilatation);
	virtual void BeginDestroy() override;

	UFUNCTION(BlueprintCallable, Category = "Tween")
		void Pause();
	UFUNCTION(BlueprintCallable, Category = "Tween")
		void Unpause();
	UFUNCTION(BlueprintCallable, Category = "Tween")
		void Restart();
	UFUNCTION(BlueprintCallable, Category = "Tween")
		void Stop();
	UFUNCTION(BlueprintCallable, Category = "Tween")
		void SetTimeMultiplier(float Multiplier);
};
