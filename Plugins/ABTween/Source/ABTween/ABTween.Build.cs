// Copyright Epic Games, Inc. All Rights Reserved.
using System.IO;
using UnrealBuildTool;

public class ABTween : ModuleRules
{
	public ABTween(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
		PrivateDependencyModuleNames.AddRange(
			new string[] { "Core", "CoreUObject", "Engine" });	//"Slate",//"SlateCore",// ... add private dependencies that you statically link with here ...}	);

		/*PublicIncludePaths.AddRange( new string[] {// ... add public include paths required here ...}	);
						
		PrivateIncludePaths.AddRange( new string[] {// ... add other private include paths required here ...});
					
		PublicDependencyModuleNames.AddRange(new string[]{"UMG"	// ... add other public dependencies that you statically link with here ...});	
		
		DynamicallyLoadedModuleNames.AddRange(new string[]{// ... add any modules that your module loads dynamically here ...}	);*/
	}
}
