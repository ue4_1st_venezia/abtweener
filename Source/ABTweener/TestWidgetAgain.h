// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TestWidgetAgain.generated.h"

/**
 * 
 */
UCLASS()
class ABTWEENER_API UTestWidgetAgain : public UUserWidget
{
	GENERATED_BODY()
public:
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* FirstText;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* SecondText;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* ThirdText;
	UPROPERTY(meta = (BindWidget))
		class UImage* FadeImage;
	UPROPERTY(EditDefaultsOnly)
		float Seconds;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bCanTween;

	UFUNCTION(BlueprintCallable)
		void StartTween();// = 0;
	UFUNCTION(BlueprintCallable)
		void ResetTranslation();// = 0;
};
