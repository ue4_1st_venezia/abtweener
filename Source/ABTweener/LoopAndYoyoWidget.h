// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseTweenWidget.h"
#include "LoopAndYoyoWidget.generated.h"

/**
 * 
 */
UCLASS()
class ABTWEENER_API ULoopAndYoyoWidget : public UBaseTweenWidget
{
	GENERATED_BODY()
	
public:
	virtual void StartTween() override;
	virtual void ResetTranslation() override;

	class UABTweener_Module* NoDelay;
	class UABTweener_Module* YoyoDelay;
	class UABTweener_Module* LoopDelay;
};
