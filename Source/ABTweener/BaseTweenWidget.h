// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "BaseTweenWidget.generated.h"

/**
 * 
 */
UCLASS()
class ABTWEENER_API UBaseTweenWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* FirstText;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* SecondText;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* ThirdText;
	UPROPERTY(EditDefaultsOnly)
		float Seconds;

	UFUNCTION(BlueprintCallable)
		virtual void StartTween();// = 0;
	UFUNCTION(BlueprintCallable)
		virtual void ResetTranslation();// = 0;
};
