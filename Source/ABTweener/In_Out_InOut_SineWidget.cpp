// Fill out your copyright notice in the Description page of Project Settings.


#include "In_Out_InOut_SineWidget.h"
#include "Components/TextBlock.h"
#include "ABTweener_Module.h"

void UIn_Out_InOut_SineWidget::StartTween()
{
	FVector2D FirstStart = FirstText->RenderTransform.Translation;
	FVector2D SecondStart = SecondText->RenderTransform.Translation;
	FVector2D ThirdStart = ThirdText->RenderTransform.Translation;
	FVector2D End = FVector2D(800, 0);

	UABTweener_Module::Play(FirstStart, FirstStart + End, [&](FVector2D t) {FirstText->SetRenderTranslation(t); }, Seconds, ETweenEase::EaseInSin);
	UABTweener_Module::Play(SecondStart, SecondStart + End, [&](FVector2D y) {SecondText->SetRenderTranslation(y); }, Seconds, ETweenEase::EaseOutSine);
	UABTweener_Module::Play(ThirdStart, ThirdStart + End, [&](FVector2D y) {ThirdText->SetRenderTranslation(y); }, Seconds, ETweenEase::EaseInOutSine);
}

void UIn_Out_InOut_SineWidget::ResetTranslation()
{
	Super::ResetTranslation();
}
