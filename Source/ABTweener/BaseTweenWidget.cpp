// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseTweenWidget.h"
#include "Components/TextBlock.h"

void UBaseTweenWidget::StartTween()
{
}

void UBaseTweenWidget::ResetTranslation()
{
	FirstText->SetRenderTranslation(FVector2D(0, 0));
	SecondText->SetRenderTranslation(FVector2D(0, 0));
	ThirdText->SetRenderTranslation(FVector2D(0, 0));
}
