// Fill out your copyright notice in the Description page of Project Settings.


#include "FadeWidget.h"
#include "Components/Image.h"
#include "ABTweener_Module.h"

void UFadeWidget::NativeConstruct()
{
	Super::NativeConstruct();

	//Per fare il fade: Prendere come INIZIO l'Alpha della ColorAndOpacity dell'oggetto e come FINE l'Alpha dell'oggetto sottraendo 1,
		//					quando si fa partire il tween mettere INIZIO e FINE, mentre nella funzione Lambda si deve creare una nuova 
		//					struct di FLinearColor, assegnare a RGB i valori che si vogliono usare mentre alla A il valore del float nella 
		//					dichiarazione, "ESEMPIO: Color.R = 0; Color.G = 0; Color.B = 0; Color.A = y;".
		//					Poi si assegna all'Alpha della ColorAndOpacity dell'oggetto il valore in dichiarazione, "ESEMPIO:FadeImage->ColorAndOpacity.A = y"
		//					Quindi alla fine si setta la SetColorAndOpacity con la ColorAndOpacity dell'oggetto.
		//					Infine si finisce il tween.
	float FadeStart = FadeImage->ColorAndOpacity.A;
	float FadeEnd = FadeImage->ColorAndOpacity.A + 1;

	UABTweener_Module::Play(FadeStart, FadeEnd, [&](float y)
	{
		FLinearColor Color;
		Color.R = 0; Color.G = 0; Color.B = 0; Color.A = y;
		FadeImage->ColorAndOpacity.A = y;
		FadeImage->SetColorAndOpacity(FadeImage->ColorAndOpacity);
	},
		Seconds, ETweenEase::EaseLinear);
}	