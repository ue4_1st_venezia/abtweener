// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "VariousMenuWidget.generated.h"

/**
 *
 */
UCLASS()
class ABTWEENER_API UVariousMenuWidget : public UUserWidget
{
	GENERATED_BODY()

private:
	void NativeConstruct() override;

	UPROPERTY()
		class APlayerController* Owner;
public:

	FTimerHandle Delay;

	void TouchCPPButtons(UWidget* Widget, ESlateVisibility MyVisibility);

	#pragma region MainMenu
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UWidgetSwitcher* WidgetSwitcher;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UButton* MobileCPPButton;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UButton* EightBitCPPButton;
	UFUNCTION()
		void MobileOnClick();
	UFUNCTION()
		void MobileReturnOnClick();
	UFUNCTION()
		void EigthBitOnClick();
	UFUNCTION()
		void EigthBitReturnOnClick();
	UFUNCTION()
		void AllCPPBackButtonOnClick();
	#pragma endregion

	#pragma region Mobile Menu
	UPROPERTY(meta = (BindWidget))
		class UCanvasPanel* MobileCanvas;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UImage* SlowCloud1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UImage* SlowCloud2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UImage* FastCloud1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UImage* FastCloud2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UImage* BoxImage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* MobileText;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* MobileInfoDescriptionText;
	UPROPERTY(meta = (BindWidget))
		class UButton* MobileCPPInfoButton;
	UPROPERTY(meta = (BindWidget))
		class UButton* MobileCPPBackButton;
	UPROPERTY(meta = (BindWidget))
		class UButton* MobileCPPInfoReturnButton;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UProgressBar* MobileFirstStar;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UProgressBar* MobileSecondStar;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UProgressBar* MobileThirdStar;
	UFUNCTION()
		void ResetMobileCPPStats();
	UFUNCTION()
		void StartMobileCPPTween();
	UFUNCTION()
		void AppairMobileCPPTextTween();
	UFUNCTION()
		void AppairMobileCPPInfoButton();
	UFUNCTION()
		void AppairMobileCPPBackButton();
	UFUNCTION()
		void ReverseMobileCPPInfoButton();
	UFUNCTION()
		void ReverseMobileCPPBackButton();
	UFUNCTION()
		void MobileInfoButtonOnHover();
	UFUNCTION()
		void MobileInfoButtonUnhover();
	UFUNCTION()
		void MobileBackButtonOnHover();
	UFUNCTION()
		void MobileBackButtonOnUnhover();
	UFUNCTION()
		void MobileInfoButtonOnClick();
	UFUNCTION()
		void AppairCPPBox();
	UFUNCTION()
		void AppairCPPFirstStar();
	UFUNCTION()
		void AppairCPPSecondStar();
	UFUNCTION()
		void AppairCPPThirdStar();
	UFUNCTION()
		void ReverseCPPFirstStar();
	UFUNCTION()
		void ReverseCPPSecondStar();
	UFUNCTION()
		void ReverseCPPThirdStar();
	UFUNCTION()
		void ReverseCPPBox();
	UFUNCTION()
		void ResetMobileButtonPos();
#pragma endregion

	#pragma region EightBitMenu
	UPROPERTY(meta = (BindWidget))
		class UCanvasPanel* EightBitCanvas;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* EightBitText;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UImage* EightBitBackground;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UImage* EightBitFirstStar;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UImage* EightBitSecondStar;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UImage* EightBitThirdStar;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UImage* EightBitFourthStar;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* EightBitInfoDescriptionText;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UImage* EightBitSpaceShipNoFire;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UImage* EightBitSpaceShipFire;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (BindWidget))
		class UImage* EightBitSpaceShipComplete;
	UPROPERTY(meta = (BindWidget))
		class UButton* EightBitCPPInfoButton;
	UPROPERTY(meta = (BindWidget))
		class UButton* EightBitCPPBackButton;
	UPROPERTY(meta = (BindWidget))
		class UButton* EightBitCPPInfoReturnButton;	
	
		class UABTweener_Module* SpaceShipFire;
	UFUNCTION()
		void ResetEigthBitCPPStats();
	UFUNCTION()
		void SpaceShipStats();
	UFUNCTION()
		void StartEigthBitCPPBackgroundTween();
	UFUNCTION()
		void StartEigthBitCPPTween();
	UFUNCTION()
		void ContinueEigthBitCPPTween();
	UFUNCTION()
		void AppairEightBitCPPCompleteSpaceship();
	UFUNCTION()
		void EigthBitInfoButtonOnHover();
	UFUNCTION()
		void EigthBitBackButtonOnHover();
	UFUNCTION()
		void EigthBitInfoButtonOnClick();
	UFUNCTION()
		void EightBitCPPMoveButtonsMoveSpaceship();
	UFUNCTION()
		void EightBitCPPMoveButtonsMoveInfoButton();
	UFUNCTION()
		void EightBitCPPMoveButtonsMoveBackButton();
	UFUNCTION()
		void MoveCPPInfoBox();
	UFUNCTION()
		void ContinueMoveCPPInfoBox();
	UFUNCTION()
		void StartReverseCPPInfoBox();
	UFUNCTION()
		void ReverseCPPInfoBox();
	UFUNCTION()
		void EndReverseCPPInfoBox();
	UFUNCTION()
		void EightBitCPPReverseSpaceShip();
	UFUNCTION()
		void EightBitCPPReverseBackButton();
	UFUNCTION()
		void EightBitCPPReverseInfoButton();
	UFUNCTION()
		void ResetEightBitButtonPos();
	UFUNCTION()
		void ResetFire();
#pragma endregion
};
