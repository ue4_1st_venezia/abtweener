// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ABTweenerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ABTWEENER_API AABTweenerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
