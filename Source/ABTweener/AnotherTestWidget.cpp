// Fill out your copyright notice in the Description page of Project Settings.


#include "AnotherTestWidget.h"
#include "Components/Image.h"
#include "ABTweener_Module.h"

void UAnotherTestWidget::StartTween()
{
	float FadeStart = FadeImage->ColorAndOpacity.A;
	float FadeEnd = FadeImage->ColorAndOpacity.A - 1;

	UABTweener_Module::Play(FadeStart, FadeEnd, [&](float y)
		{
			FLinearColor Color;
			Color.R = 0; Color.G = 0; Color.B = 0; Color.A = y;
			FadeImage->ColorAndOpacity.A = y;
			FadeImage->SetColorAndOpacity(FadeImage->ColorAndOpacity);
		},
		5, ETweenEase::EaseLinear);
}
