// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class ABTWEENER_API UMainMenuWidget : public UUserWidget
{
	GENERATED_BODY()
	
private:
	void NativeConstruct() override;

public:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UTextBlock* ABTweener_Text;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UButton* BP_Button;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		class UButton* CPP_Button;
	UPROPERTY(meta = (BindWidget))
		class UProgressBar* FirstStar;
	UPROPERTY(meta = (BindWidget))
		class UProgressBar* SecondStar;
	UPROPERTY(meta = (BindWidget))
		class UProgressBar* ThirdStar;
	UPROPERTY(meta = (BindWidget))
		class UImage* BoxImage;
	UPROPERTY(meta = (BindWidget))
		class UButton* BackButton;
	UPROPERTY()
		class APlayerController* Owner;

	UFUNCTION()
		void InitialSets();
	UFUNCTION()
		void SecondTween();
	UFUNCTION()
		void ContinueSecondTween();
	UFUNCTION()
		void A();
	UFUNCTION()
		void B();
	UFUNCTION()
		void ThirdTween();
	UFUNCTION()
		void ReverseThirdTween();
	UFUNCTION()
		void AppairFirstStarTween();
	UFUNCTION()
		void C();
	UFUNCTION()
		void ReverseFirstStarTween();
	UFUNCTION()
		void AppairSecondStarTween();
	UFUNCTION()
		void D();
	UFUNCTION()
		void ReverseSecondStarTween();
	UFUNCTION()
		void AppairThirdStarTween();
	UFUNCTION()
		void E();
	UFUNCTION()
		void ReverseThirdStarTween();
	UFUNCTION()
		void CPPButtonHover();
	UFUNCTION()
		void CPPButtonUnhover();
	UFUNCTION()
		void BackButtonClick();

};
