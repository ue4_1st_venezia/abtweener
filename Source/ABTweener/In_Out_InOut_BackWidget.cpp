// Fill out your copyright notice in the Description page of Project Settings.


#include "In_Out_InOut_BackWidget.h"
#include "Components/TextBlock.h"
#include "ABTweener_Module.h"

void UIn_Out_InOut_BackWidget::StartTween()
{
	FVector2D FirstStart = FirstText->RenderTransform.Translation;
	FVector2D SecondStart = SecondText->RenderTransform.Translation;
	FVector2D ThirdStart = ThirdText->RenderTransform.Translation;
	FVector2D End = FVector2D(600, 0);

	InBack->Play(FirstStart, FirstStart + End, [&](FVector2D t) {FirstText->SetRenderTranslation(t); }, Seconds, ETweenEase::EaseInBack);
	OutBack->Play(SecondStart, SecondStart + End, [&](FVector2D y) {SecondText->SetRenderTranslation(y); }, Seconds, ETweenEase::EaseOutBack);
	InOutBack->Play(ThirdStart, ThirdStart + End, [&](FVector2D y) {ThirdText->SetRenderTranslation(y); }, Seconds, ETweenEase::EaseInOutBack);
}

void UIn_Out_InOut_BackWidget::ResetTranslation()
{
	Super::ResetTranslation();

	InBack->ClearActiveTweens();
	OutBack->ClearActiveTweens();
	InOutBack->ClearActiveTweens();
}
