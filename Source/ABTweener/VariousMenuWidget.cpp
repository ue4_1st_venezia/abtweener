// Fill out your copyright notice in the Description page of Project Settings.


#include "VariousMenuWidget.h"
#include "Components/WidgetSwitcher.h"
#include "Components/Image.h"
#include "Components/Button.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "ABTweener_Module.h"
#include "ABTweener_Instance_Vector2D.h"

void UVariousMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();
	Owner = Cast<APlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));

#pragma region MainMenu
	MobileCPPButton->OnClicked.AddUniqueDynamic(this, &UVariousMenuWidget::MobileOnClick);
	EightBitCPPButton->OnClicked.AddUniqueDynamic(this, &UVariousMenuWidget::EigthBitOnClick);
#pragma endregion

#pragma region MobileMenu
	MobileCPPInfoButton->SetVisibility(ESlateVisibility::Hidden);
	MobileCPPBackButton->SetVisibility(ESlateVisibility::Hidden);
	MobileCPPInfoReturnButton->SetVisibility(ESlateVisibility::Hidden);

	MobileCPPInfoButton->OnHovered.AddUniqueDynamic(this, &UVariousMenuWidget::MobileInfoButtonOnHover);
	MobileCPPInfoButton->OnUnhovered.AddUniqueDynamic(this, &UVariousMenuWidget::MobileInfoButtonUnhover);
	MobileCPPBackButton->OnHovered.AddUniqueDynamic(this, &UVariousMenuWidget::MobileBackButtonOnHover);
	MobileCPPBackButton->OnUnhovered.AddUniqueDynamic(this, &UVariousMenuWidget::MobileBackButtonOnUnhover);
	MobileCPPInfoButton->OnClicked.AddUniqueDynamic(this, &UVariousMenuWidget::ReverseMobileCPPBackButton);
	MobileCPPInfoReturnButton->OnClicked.AddUniqueDynamic(this, &UVariousMenuWidget::ReverseCPPThirdStar);
	MobileCPPBackButton->OnClicked.AddUniqueDynamic(this, &UVariousMenuWidget::MobileReturnOnClick);
#pragma endregion

#pragma region EightBitMenu
	EightBitCPPInfoButton->SetVisibility(ESlateVisibility::Hidden);
	EightBitCPPBackButton->SetVisibility(ESlateVisibility::Hidden);

	EightBitCPPInfoButton->OnHovered.AddUniqueDynamic(this, &UVariousMenuWidget::EigthBitInfoButtonOnHover);
	EightBitCPPBackButton->OnHovered.AddUniqueDynamic(this, &UVariousMenuWidget::EigthBitBackButtonOnHover);
	EightBitCPPInfoButton->OnClicked.AddUniqueDynamic(this, &UVariousMenuWidget::EigthBitInfoButtonOnClick);
	EightBitCPPInfoReturnButton->OnClicked.AddUniqueDynamic(this, &UVariousMenuWidget::StartReverseCPPInfoBox);
	EightBitCPPBackButton->OnClicked.AddUniqueDynamic(this, &UVariousMenuWidget::EigthBitReturnOnClick);
#pragma endregion
}

void UVariousMenuWidget::TouchCPPButtons(UWidget* Widget, ESlateVisibility MyVisibility)
{
	Widget->SetVisibility(MyVisibility);
}

#pragma region MainMenu

#pragma region MobileOnClick
void UVariousMenuWidget::MobileOnClick()
{
	WidgetSwitcher->SetActiveWidgetIndex(1);
	ResetMobileCPPStats();
	MobileCPPInfoButton->SetVisibility(ESlateVisibility::Visible);
	MobileCPPBackButton->SetVisibility(ESlateVisibility::Visible);
	StartMobileCPPTween();
}

void UVariousMenuWidget::MobileReturnOnClick()
{
	ResetMobileButtonPos();
	ResetMobileCPPStats();
	AllCPPBackButtonOnClick();
}
#pragma endregion

#pragma region EightBitOnClick
void UVariousMenuWidget::EigthBitOnClick()
{
	WidgetSwitcher->SetActiveWidgetIndex(2);
	ResetEigthBitCPPStats();
	StartEigthBitCPPBackgroundTween();
	StartEigthBitCPPTween();
}
void UVariousMenuWidget::EigthBitReturnOnClick()
{
	ResetEightBitButtonPos();
	ResetFire();
	ResetEigthBitCPPStats();
	AllCPPBackButtonOnClick();
}
#pragma endregion

#pragma region AllCPPButtonClick
void UVariousMenuWidget::AllCPPBackButtonOnClick()
{
	WidgetSwitcher->SetActiveWidgetIndex(0);
}
#pragma endregion
#pragma endregion

#pragma region MobileMenu

#pragma region ResetMobileCPPStats
void UVariousMenuWidget::ResetMobileCPPStats()
{
	FVector2D Zero = FVector2D(0, 0);
	SlowCloud1->SetRenderTranslation(Zero);
	SlowCloud2->SetRenderTranslation(Zero);
	FastCloud1->SetRenderTranslation(Zero);
	FastCloud2->SetRenderTranslation(Zero);
	MobileText->SetRenderScale(Zero);
	MobileCPPInfoButton->SetRenderTranslation(Zero);
	MobileCPPBackButton->SetRenderTranslation(Zero);
	BoxImage->SetRenderScale(FVector2D(1, 0));
	MobileCPPInfoButton->SetRenderScale(Zero);
	MobileCPPBackButton->SetRenderScale(Zero);
}
#pragma endregion

#pragma region StartCPPTween
void UVariousMenuWidget::StartMobileCPPTween()
{
	FVector2D SlowCloud1Start = SlowCloud1->RenderTransform.Translation;
	FVector2D SlowCloud1End = FVector2D(5000, 0);
	UABTweener_Module::Play(SlowCloud1Start, SlowCloud1Start + SlowCloud1End, [&](FVector2D t) {SlowCloud1->SetRenderTranslation(t); }, 30, ETweenEase::EaseLinear)->SetLoops(-1)->SetLoopDelay(3);

	FVector2D SlowCloud2Start = SlowCloud2->RenderTransform.Translation;
	FVector2D SlowCloud2End = FVector2D(4000, 0);
	UABTweener_Module::Play(SlowCloud2Start, SlowCloud2Start + SlowCloud2End, [&](FVector2D y) {SlowCloud2->SetRenderTranslation(y); }, 35, ETweenEase::EaseLinear)->SetLoops(-1)->SetLoopDelay(2);

	FVector2D FastCloud1Start = FastCloud1->RenderTransform.Translation;
	FVector2D FastCloud1End = FVector2D(4000, 0);
	UABTweener_Module::Play(FastCloud1Start, FastCloud1Start + FastCloud1End, [&](FVector2D u) {FastCloud1->SetRenderTranslation(u); }, 10, ETweenEase::EaseLinear)->SetLoops(-1)->SetLoopDelay(4);

	FVector2D FastCloud2Start = FastCloud2->RenderTransform.Translation;
	FVector2D FastCloud2End = FVector2D(4000, 0);
	UABTweener_Module::Play(FastCloud2Start, FastCloud2Start + FastCloud2End, [&](FVector2D i) {FastCloud2->SetRenderTranslation(i); }, 8, ETweenEase::EaseLinear)->SetLoops(-1)->SetLoopDelay(5);

	AppairMobileCPPTextTween();
}
#pragma endregion

void UVariousMenuWidget::AppairMobileCPPTextTween()
{
	FVector2D Start = MobileText->RenderTransform.Scale;
	FVector2D End = FVector2D(1,1);

	UABTweener_Module::Play(Start, Start + End, [&](FVector2D t) {MobileText->SetRenderScale(t); }, 4, ETweenEase::EaseOutBounce)->SetOnCompleted([&](void) {AppairMobileCPPInfoButton(); });
}

#pragma region AppairCPPMenuButton
void UVariousMenuWidget::AppairMobileCPPInfoButton()
{
	TouchCPPButtons(MobileCPPInfoButton, ESlateVisibility::SelfHitTestInvisible);

	FVector2D InfoButtonStart = MobileCPPInfoButton->RenderTransform.Scale;
	FVector2D ButtonEnd = FVector2D(1, 1);

	UABTweener_Module::Play(InfoButtonStart, InfoButtonStart + ButtonEnd, [&](FVector2D t) {MobileCPPInfoButton->SetRenderScale(t); }, 1, ETweenEase::EaseOutBounce)
						->SetOnCompleted([&](void) {TouchCPPButtons(MobileCPPInfoButton, ESlateVisibility::Visible); });

	Owner->GetWorldTimerManager().SetTimer(Delay, this, &UVariousMenuWidget::AppairMobileCPPBackButton, .5f, false);
}

void UVariousMenuWidget::AppairMobileCPPBackButton()
{
	TouchCPPButtons(MobileCPPBackButton, ESlateVisibility::SelfHitTestInvisible);

	FVector2D BackButtonStart = MobileCPPBackButton->RenderTransform.Scale;
	FVector2D ButtonEnd = FVector2D(1, 1);

	UABTweener_Module::Play(BackButtonStart, BackButtonStart + ButtonEnd, [&](FVector2D y) {MobileCPPBackButton->SetRenderScale(y); }, 1, ETweenEase::EaseOutBounce)
				->SetOnCompleted([&](void) {TouchCPPButtons(MobileCPPBackButton, ESlateVisibility::Visible); });
}
#pragma endregion

#pragma region ReverseCPPMenuButton
void UVariousMenuWidget::ReverseMobileCPPBackButton()
{
	TouchCPPButtons(MobileCPPBackButton, ESlateVisibility::SelfHitTestInvisible);

	FVector2D BackButtonStart = MobileCPPBackButton->RenderTransform.Scale;
	FVector2D ButtonEnd = FVector2D(1, 1);

	UABTweener_Module::Play(BackButtonStart, BackButtonStart - ButtonEnd, [&](FVector2D y) {MobileCPPBackButton->SetRenderScale(y); }, 1, ETweenEase::EaseOutBounce);
	Owner->GetWorldTimerManager().SetTimer(Delay, this, &UVariousMenuWidget::ReverseMobileCPPInfoButton, .5f, false);
}

void UVariousMenuWidget::ReverseMobileCPPInfoButton()
{
	TouchCPPButtons(MobileCPPBackButton, ESlateVisibility::SelfHitTestInvisible);

	FVector2D InfoButtonStart = MobileCPPInfoButton->RenderTransform.Scale;
	FVector2D ButtonEnd = FVector2D(1, 1);

	UABTweener_Module::Play(InfoButtonStart, InfoButtonStart - ButtonEnd, [&](FVector2D t) {MobileCPPInfoButton->SetRenderScale(t); }, 1, ETweenEase::EaseOutBounce)
		->SetOnCompleted([&](void){MobileCPPInfoButton->SetRenderScale(FVector2D(0, 0));
								   MobileCPPBackButton->SetRenderScale(FVector2D(0, 0)); 
								   Owner->GetWorldTimerManager().SetTimer(Delay, this, &UVariousMenuWidget::AppairCPPBox, .5f, false);});
}
#pragma endregion

#pragma region CPPMenuButtonOnHoverAndOnUnhover
void UVariousMenuWidget::MobileInfoButtonOnHover()
{
	FVector2D ButtonStart = MobileCPPInfoButton->RenderTransform.Scale;
	FVector2D ButtonEnd = FVector2D(.2f, .2f);

	UABTweener_Module::Play(ButtonStart, ButtonStart + ButtonEnd, [&](FVector2D t) {MobileCPPInfoButton->SetRenderScale(t); }, 1, ETweenEase::EaseOutBounce);
}

void UVariousMenuWidget::MobileInfoButtonUnhover()
{
	FVector2D ButtonStart = MobileCPPInfoButton->RenderTransform.Scale;
	FVector2D ButtonEnd = FVector2D(-.2f, -.2f);


	UABTweener_Module::Play(ButtonStart, ButtonStart + ButtonEnd, [&](FVector2D t) {MobileCPPInfoButton->SetRenderScale(t); }, 1, ETweenEase::EaseOutBounce);
}

void UVariousMenuWidget::MobileBackButtonOnHover()
{
	FVector2D ButtonStart = MobileCPPBackButton->RenderTransform.Scale;
	FVector2D ButtonEnd = FVector2D(.2f, .2f);

	UABTweener_Module::Play(ButtonStart, ButtonStart + ButtonEnd, [&](FVector2D t) {MobileCPPBackButton->SetRenderScale(t); }, 1, ETweenEase::EaseOutBounce);
}

void UVariousMenuWidget::MobileBackButtonOnUnhover()
{
	FVector2D ButtonStart = MobileCPPBackButton->RenderTransform.Scale;
	FVector2D ButtonEnd = FVector2D(-.2f, -.2f);

	UABTweener_Module::Play(ButtonStart, ButtonStart + ButtonEnd, [&](FVector2D t) {MobileCPPBackButton->SetRenderScale(t); }, 1, ETweenEase::EaseOutBounce);
}
#pragma endregion

#pragma region OnInfoButtonClick
void UVariousMenuWidget::MobileInfoButtonOnClick()
{
	ReverseMobileCPPBackButton();
	Owner->GetWorldTimerManager().SetTimer(Delay, this, &UVariousMenuWidget::AppairCPPBox, .5f, false);
}
#pragma endregion

#pragma region AppairBox
void UVariousMenuWidget::AppairCPPBox()
{
	FVector2D BoxStart = BoxImage->RenderTransform.Scale;
	FVector2D TextStart = MobileInfoDescriptionText->RenderTransform.Scale;
	FVector2D End = FVector2D(0, 1);

	UABTweener_Module::Play(BoxStart, BoxStart + End, [&](FVector2D t) {BoxImage->SetRenderScale(t); }, 2, ETweenEase::EaseInOutSine);
	UABTweener_Module::Play(TextStart, TextStart + End, [&](FVector2D y) {MobileInfoDescriptionText->SetRenderScale(y); }, 2, ETweenEase::EaseInOutSine)->SetOnCompleted([&](void) {AppairCPPFirstStar(); });
}
#pragma endregion

#pragma region AppairStars
void UVariousMenuWidget::AppairCPPFirstStar()
{
	FVector2D ScaleStart = MobileFirstStar->RenderTransform.Scale;
	FVector2D ScaleEnd = FVector2D(1, 1);
	UABTweener_Module::Play(ScaleStart, ScaleStart + ScaleEnd, [&](FVector2D t) {MobileFirstStar->SetRenderScale(t); }, 1, ETweenEase::EaseLinear);

	float AngleStart = MobileFirstStar->RenderTransform.Angle;
	float AngleEnd = 210;
	UABTweener_Module::Play(AngleStart, AngleStart - AngleEnd, [&](float y) {MobileFirstStar->SetRenderTransformAngle(y); }, 1, ETweenEase::EaseInOutCirc)
		->SetOnCompleted([&](void) {

		float PercentStart = MobileFirstStar->Percent;
		float PercentEnd = 1;
		UABTweener_Module::Play(PercentStart, PercentStart + PercentEnd, [&](float u) {MobileFirstStar->SetPercent(u); }, 1, ETweenEase::EaseLinear);
	});

	Owner->GetWorldTimerManager().SetTimer(Delay, this, &UVariousMenuWidget::AppairCPPSecondStar, 1.f, false);
}

void UVariousMenuWidget::AppairCPPSecondStar()
{
	FVector2D ScaleStart = MobileSecondStar->RenderTransform.Scale;
	FVector2D ScaleEnd = FVector2D(1, 1);
	UABTweener_Module::Play(ScaleStart, ScaleStart + ScaleEnd, [&](FVector2D t) {MobileSecondStar->SetRenderScale(t); }, 1, ETweenEase::EaseLinear);

	float AngleStart = MobileSecondStar->RenderTransform.Angle;
	float AngleEnd = 215;
	UABTweener_Module::Play(AngleStart, AngleStart - AngleEnd, [&](float y) {MobileSecondStar->SetRenderTransformAngle(y); }, 1, ETweenEase::EaseInOutCirc)
		->SetOnCompleted([&](void) {

		float PercentStart = MobileSecondStar->Percent;
		float PercentEnd = 1;
		UABTweener_Module::Play(PercentStart, PercentStart + PercentEnd, [&](float u) {MobileSecondStar->SetPercent(u); }, 1, ETweenEase::EaseLinear);
	});

	Owner->GetWorldTimerManager().SetTimer(Delay, this, &UVariousMenuWidget::AppairCPPThirdStar, 1.f, false);
}

void UVariousMenuWidget::AppairCPPThirdStar()
{
	FVector2D ScaleStart = MobileThirdStar->RenderTransform.Scale;
	FVector2D ScaleEnd = FVector2D(1, 1);
	UABTweener_Module::Play(ScaleStart, ScaleStart + ScaleEnd, [&](FVector2D t) {MobileThirdStar->SetRenderScale(t); }, 1, ETweenEase::EaseLinear);

	float AngleStart = MobileThirdStar->RenderTransform.Angle;
	float AngleEnd = 210;
	UABTweener_Module::Play(AngleStart, AngleStart - AngleEnd, [&](float y) {MobileThirdStar->SetRenderTransformAngle(y); }, 1, ETweenEase::EaseInOutCirc)
		->SetOnCompleted([&](void) {

		float PercentStart = MobileThirdStar->Percent;
		float PercentEnd = 1;
		UABTweener_Module::Play(PercentStart, PercentStart + PercentEnd, [&](float u) {MobileThirdStar->SetPercent(u); }, 1, ETweenEase::EaseLinear)
			->SetOnCompleted([&](void) {
			MobileCPPInfoReturnButton->SetVisibility(ESlateVisibility::Visible); });
	});
}
#pragma endregion

#pragma region ReverseStars
void UVariousMenuWidget::ReverseCPPThirdStar()
{
	MobileCPPInfoReturnButton->SetVisibility(ESlateVisibility::Hidden);

	FVector2D ScaleStart = MobileThirdStar->RenderTransform.Scale;
	FVector2D ScaleEnd = FVector2D(1, 1);
	UABTweener_Module::Play(ScaleStart, ScaleStart - ScaleEnd, [&](FVector2D t) {MobileThirdStar->SetRenderScale(t); }, 1, ETweenEase::EaseLinear);

	float AngleStart = MobileThirdStar->RenderTransform.Angle;
	float AngleEnd = 210;
	UABTweener_Module::Play(AngleStart, AngleStart + AngleEnd, [&](float y) {MobileThirdStar->SetRenderTransformAngle(y); }, 1, ETweenEase::EaseInOutCirc)
		->SetOnCompleted([&](void) {

		float PercentStart = MobileThirdStar->Percent;
		float PercentEnd = 1;
		UABTweener_Module::Play(PercentStart, PercentStart - PercentEnd, [&](float u) {MobileThirdStar->SetPercent(u); }, 1, ETweenEase::EaseLinear);

		MobileThirdStar->SetRenderTransformAngle(-180);
	});

	Owner->GetWorldTimerManager().SetTimer(Delay, this, &UVariousMenuWidget::ReverseCPPSecondStar, 1.f, false);
}

void UVariousMenuWidget::ReverseCPPSecondStar()
{
	FVector2D ScaleStart = MobileSecondStar->RenderTransform.Scale;
	FVector2D ScaleEnd = FVector2D(1, 1);
	UABTweener_Module::Play(ScaleStart, ScaleStart - ScaleEnd, [&](FVector2D t) {MobileSecondStar->SetRenderScale(t); }, 1, ETweenEase::EaseLinear);

	float AngleStart = MobileSecondStar->RenderTransform.Angle;
	float AngleEnd = 215;
	UABTweener_Module::Play(AngleStart, AngleStart + AngleEnd, [&](float y) {MobileSecondStar->SetRenderTransformAngle(y); }, 1, ETweenEase::EaseInOutCirc)
		->SetOnCompleted([&](void) {

		float PercentStart = MobileSecondStar->Percent;
		float PercentEnd = 1;
		UABTweener_Module::Play(PercentStart, PercentStart - PercentEnd, [&](float u) {MobileSecondStar->SetPercent(u); }, 1, ETweenEase::EaseLinear);

		MobileSecondStar->SetRenderTransformAngle(215);
	});

	Owner->GetWorldTimerManager().SetTimer(Delay, this, &UVariousMenuWidget::ReverseCPPFirstStar, 1.f, false);
}

void UVariousMenuWidget::ReverseCPPFirstStar()
{
	FVector2D ScaleStart = MobileFirstStar->RenderTransform.Scale;
	FVector2D ScaleEnd = FVector2D(1, 1);
	UABTweener_Module::Play(ScaleStart, ScaleStart - ScaleEnd, [&](FVector2D t) {MobileFirstStar->SetRenderScale(t); }, 1, ETweenEase::EaseLinear);

	float AngleStart = MobileFirstStar->RenderTransform.Angle;
	float AngleEnd = 180;
	UABTweener_Module::Play(AngleStart, AngleStart + AngleEnd, [&](float y) {MobileFirstStar->SetRenderTransformAngle(y); }, 1, ETweenEase::EaseInOutCirc)
		->SetOnCompleted([&](void) {

		float PercentStart = MobileFirstStar->Percent;
		float PercentEnd = 1;
		UABTweener_Module::Play(PercentStart, PercentStart - PercentEnd, [&](float u) {MobileFirstStar->SetPercent(u); }, 1, ETweenEase::EaseLinear);

		MobileFirstStar->SetRenderTransformAngle(180);
	});

	Owner->GetWorldTimerManager().SetTimer(Delay, this, &UVariousMenuWidget::ReverseCPPBox, .5f, false);
}
#pragma endregion

#pragma region ReverseCPPBox
void UVariousMenuWidget::ReverseCPPBox()
{
	FVector2D BoxStart = BoxImage->RenderTransform.Scale;
	FVector2D TextStart = MobileInfoDescriptionText->RenderTransform.Scale;
	FVector2D End = FVector2D(0, 1);

	UABTweener_Module::Play(BoxStart, BoxStart - End, [&](FVector2D t) {BoxImage->SetRenderScale(t); }, 2, ETweenEase::EaseInOutSine);
	UABTweener_Module::Play(TextStart, TextStart - End, [&](FVector2D y) {MobileInfoDescriptionText->SetRenderScale(y); }, 2, ETweenEase::EaseInOutSine)->SetOnCompleted([&](void) {AppairMobileCPPInfoButton(); });
}
#pragma endregion

void UVariousMenuWidget::ResetMobileButtonPos()
{
	FVector2D Zero = FVector2D(0, 0);
	MobileCPPInfoButton->SetRenderScale(Zero);
	MobileCPPBackButton->SetRenderScale(Zero);
	MobileCPPInfoButton->SetRenderTranslation(Zero);
	MobileCPPBackButton->SetRenderTranslation(Zero);
	MobileCPPInfoButton->SetVisibility(ESlateVisibility::Hidden);
	MobileCPPBackButton->SetVisibility(ESlateVisibility::Hidden);

	MobileFirstStar->SetRenderScale(Zero);
	MobileFirstStar->SetRenderTransformAngle(0);
	MobileFirstStar->SetPercent(0);

	MobileSecondStar->SetRenderScale(Zero);
	MobileSecondStar->SetRenderTransformAngle(0);
	MobileSecondStar->SetPercent(0);

	MobileThirdStar->SetRenderScale(Zero);
	MobileThirdStar->SetRenderTransformAngle(0);
	MobileThirdStar->SetPercent(0);
}
#pragma endregion

#pragma region EightBitMenu
#pragma region ResetEightBitCPPStats
void UVariousMenuWidget::ResetEigthBitCPPStats()
{
	EightBitText->SetRenderTranslation(FVector2D(-4000, 0));
	EightBitSpaceShipFire->SetRenderTranslation(FVector2D(-3000, -250));
	EightBitSpaceShipNoFire->SetRenderTranslation(FVector2D(-3000, -250));
	EightBitSpaceShipFire->SetRenderScale(FVector2D(2, 2));
	EightBitSpaceShipNoFire->SetRenderScale(FVector2D(2, 2));
	EightBitSpaceShipFire->SetRenderTransformAngle(0);
	EightBitSpaceShipNoFire->SetRenderTransformAngle(0);
	EightBitCPPInfoButton->SetVisibility(ESlateVisibility::Hidden);
	EightBitCPPBackButton->SetVisibility(ESlateVisibility::Hidden);
	EightBitSpaceShipComplete->SetRenderTranslation(FVector2D(-1500, 0));
	EightBitBackground->SetRenderTranslation(FVector2D(460, 0));
	EightBitFirstStar->SetColorAndOpacity(FLinearColor::White);
	EightBitSecondStar->SetColorAndOpacity(FLinearColor::White);
	EightBitThirdStar->SetColorAndOpacity(FLinearColor::White);
	EightBitFourthStar->SetColorAndOpacity(FLinearColor::White);
}
#pragma endregion

#pragma region SpaceShipStats
void UVariousMenuWidget::SpaceShipStats()
{
	EightBitSpaceShipFire->SetRenderScale(FVector2D(-4, -4));
	EightBitSpaceShipNoFire->SetRenderScale(FVector2D(-4, -4));
	EightBitSpaceShipFire->SetRenderTransformAngle(-10);
	EightBitSpaceShipNoFire->SetRenderTransformAngle(-10);
}
#pragma endregion

#pragma region CompleteInitialEightBitTween
void UVariousMenuWidget::StartEigthBitCPPBackgroundTween()
{
	FVector2D BGStart = EightBitBackground->RenderTransform.Translation;
	FVector2D BGEnd = FVector2D(1450, 0);

	UABTweener_Module::Play(BGStart, BGStart - BGEnd, [&](FVector2D t) {EightBitBackground->SetRenderTranslation(t); }, 10, ETweenEase::EaseLinear)->SetLoops(-1);

	float FirstStarColor = EightBitFirstStar->ColorAndOpacity.G;
	float SecondStarColor = EightBitSecondStar->ColorAndOpacity.R;
	float ThirdStarColor = EightBitThirdStar->ColorAndOpacity.B;
	float FourthStarColor = EightBitFourthStar->ColorAndOpacity.R;
	float End = 0.9f;

	UABTweener_Module::Play(FirstStarColor, FirstStarColor - End, [&](float t)
	{
		FLinearColor NewColor;
		NewColor.R = EightBitFirstStar->ColorAndOpacity.R = 1;
		NewColor.B = EightBitFirstStar->ColorAndOpacity.B = 1;
		NewColor.A = EightBitFirstStar->ColorAndOpacity.A = 1;
		NewColor.G = t;

		EightBitFirstStar->SetColorAndOpacity(NewColor);
	}, 0.5, ETweenEase::EaseInOutSine)->SetYoyo(true)->SetLoops(-1);

	UABTweener_Module::Play(SecondStarColor, SecondStarColor - End, [&](float y)
	{
		FLinearColor NewColor;
		NewColor.R = y;
		NewColor.B = EightBitSecondStar->ColorAndOpacity.B = 1;
		NewColor.A = EightBitSecondStar->ColorAndOpacity.A = 1;
		NewColor.G = EightBitSecondStar->ColorAndOpacity.G = 1;;

		EightBitSecondStar->SetColorAndOpacity(NewColor);
	}, 0.5, ETweenEase::EaseInOutSine)->SetYoyo(true)->SetLoops(-1);

	UABTweener_Module::Play(ThirdStarColor, ThirdStarColor - End, [&](float u)
	{
		FLinearColor NewColor;
		NewColor.R = EightBitThirdStar->ColorAndOpacity.R = 1;
		NewColor.B = u;
		NewColor.A = EightBitThirdStar->ColorAndOpacity.A = 1;
		NewColor.G = EightBitThirdStar->ColorAndOpacity.B = 1;

		EightBitThirdStar->SetColorAndOpacity(NewColor); 
	}, 0.5, ETweenEase::EaseInOutSine)->SetYoyo(true)->SetLoops(-1);

	UABTweener_Module::Play(FourthStarColor, FourthStarColor - End, [&](float i)
	{
		FLinearColor NewColor;
		NewColor.R = i;
		NewColor.B = EightBitFourthStar->ColorAndOpacity.B = 1;
		NewColor.A = EightBitFourthStar->ColorAndOpacity.A = 1;
		NewColor.G = EightBitFourthStar->ColorAndOpacity.G = 1;

		EightBitFourthStar->SetColorAndOpacity(NewColor); 
	}, 0.5, ETweenEase::EaseInOutSine)->SetYoyo(true)->SetLoops(-1);
}

void UVariousMenuWidget::StartEigthBitCPPTween()
{
	FVector2D SpaceShipNFStart = EightBitSpaceShipNoFire->RenderTransform.Translation;
	FVector2D SpaceShipFStart = EightBitSpaceShipFire->RenderTransform.Translation;
	FVector2D SpaceShipEnd = FVector2D(6000, 0);

	UABTweener_Module::Play(SpaceShipNFStart, SpaceShipNFStart + SpaceShipEnd, [&](FVector2D u) {EightBitSpaceShipNoFire->SetRenderTranslation(u); }, 5, ETweenEase::EaseInQuint);
	UABTweener_Module::Play(SpaceShipFStart, SpaceShipFStart + SpaceShipEnd, [&](FVector2D i) {EightBitSpaceShipFire->SetRenderTranslation(i); }, 5, ETweenEase::EaseInQuint)
		->SetOnCompleted([&](void) {SpaceShipStats(); ContinueEigthBitCPPTween(); });

	FVector2D SpaceShipFScaleStart = EightBitSpaceShipFire->RenderTransform.Scale;
	FVector2D SpaceShipFScaleEnd = FVector2D(0, 0.5f);

	UABTweener_Module::Play(SpaceShipFScaleStart, SpaceShipFScaleStart + SpaceShipFScaleEnd, [&](FVector2D o) {EightBitSpaceShipNoFire->SetRenderScale(o); }, .5f, ETweenEase::EaseSmoothstep)
		->SetYoyo(true)->SetLoops(-1);

	FVector2D TextStart = EightBitText->RenderTransform.Translation;
	FVector2D TextEnd = FVector2D(4000, 0);
	UABTweener_Module::Play(TextStart, TextStart + TextEnd, [&](FVector2D p) {EightBitText->SetRenderTranslation(p); }, 5, ETweenEase::EaseInQuint);
}

void UVariousMenuWidget::ContinueEigthBitCPPTween()
{
	FVector2D SpaceShipNFStart = EightBitSpaceShipNoFire->RenderTransform.Translation;
	FVector2D SpaceShipFStart = EightBitSpaceShipFire->RenderTransform.Translation;
	FVector2D SpaceShipEnd = FVector2D(5000, -650);

	UABTweener_Module::Play(SpaceShipNFStart, SpaceShipNFStart - SpaceShipEnd, [&](FVector2D t) {EightBitSpaceShipNoFire->SetRenderTranslation(t); }, 3, ETweenEase::EaseInQuint);
	UABTweener_Module::Play(SpaceShipFStart, SpaceShipFStart - SpaceShipEnd, [&](FVector2D t) {EightBitSpaceShipFire->SetRenderTranslation(t); }, 3, ETweenEase::EaseInQuint)
		->SetOnCompleted([&](void) {AppairEightBitCPPCompleteSpaceship(); });

	FVector2D SpaceShipFScaleStart = EightBitSpaceShipFire->RenderTransform.Scale;
	FVector2D SpaceShipFScaleEnd = FVector2D(0, 0.5f);

	SpaceShipFire->Play(SpaceShipFScaleStart, SpaceShipFScaleStart + SpaceShipFScaleEnd, [&](FVector2D o) {EightBitSpaceShipNoFire->SetRenderScale(o); }, .5f, ETweenEase::EaseSmoothstep)
		->SetYoyo(true)->SetLoops(-1);
}

void UVariousMenuWidget::AppairEightBitCPPCompleteSpaceship()
{
	FVector2D Start = EightBitSpaceShipComplete->RenderTransform.Translation;
	FVector2D End = FVector2D(1500, 0);

	UABTweener_Module::Play(Start, Start + End, [&](FVector2D t) {EightBitSpaceShipComplete->SetRenderTranslation(t); }, 3, ETweenEase::EaseInOutQuint)
		->SetOnCompleted([&](void) {
									EightBitCPPInfoButton->SetVisibility(ESlateVisibility::Visible); 
									EightBitCPPBackButton->SetVisibility(ESlateVisibility::Visible);
									});
}
#pragma endregion

#pragma region Info and BackButton OnHover
void UVariousMenuWidget::EigthBitInfoButtonOnHover()
{
	EightBitSpaceShipComplete->SetRenderTranslation(FVector2D(0, 0));
}

void UVariousMenuWidget::EigthBitBackButtonOnHover()
{
	EightBitSpaceShipComplete->SetRenderTranslation(FVector2D(0, 100));
}
#pragma endregion

#pragma region EightBitInfoButtonOnClick
void UVariousMenuWidget::EigthBitInfoButtonOnClick()
{
	EightBitCPPMoveButtonsMoveSpaceship();
}
#pragma endregion

#pragma region MoveButtons
void UVariousMenuWidget::EightBitCPPMoveButtonsMoveSpaceship()
{
	FVector2D Start = EightBitSpaceShipComplete->RenderTransform.Translation;
	FVector2D End = FVector2D(1500, 0);

	UABTweener_Module::Play(Start, Start + End, [&](FVector2D t) {EightBitSpaceShipComplete->SetRenderTranslation(t); }, 3, ETweenEase::EaseInOutQuint);

	Owner->GetWorldTimerManager().SetTimer(Delay, this, &UVariousMenuWidget::EightBitCPPMoveButtonsMoveInfoButton, .5f, false);
}

void UVariousMenuWidget::EightBitCPPMoveButtonsMoveInfoButton()
{
	FVector2D Start = EightBitCPPInfoButton->RenderTransform.Translation;
	FVector2D End = FVector2D(1500, 0);

	UABTweener_Module::Play(Start, Start + End, [&](FVector2D t) {EightBitCPPInfoButton->SetRenderTranslation(t); }, 3, ETweenEase::EaseInOutQuint);

	Owner->GetWorldTimerManager().SetTimer(Delay, this, &UVariousMenuWidget::EightBitCPPMoveButtonsMoveBackButton, .5f, false);
}

void UVariousMenuWidget::EightBitCPPMoveButtonsMoveBackButton()
{
	FVector2D Start = EightBitCPPBackButton->RenderTransform.Translation;
	FVector2D End = FVector2D(1500, 0);

	UABTweener_Module::Play(Start, Start + End, [&](FVector2D t) {EightBitCPPBackButton->SetRenderTranslation(t); }, 3, ETweenEase::EaseInOutQuint)->SetOnCompleted([&](void) {MoveCPPInfoBox(); });
}
#pragma endregion

#pragma region MoveCPPInfoBox
void UVariousMenuWidget::MoveCPPInfoBox()
{
	EightBitSpaceShipComplete->SetRenderScale(FVector2D(-.5, -.5));

	FVector2D Start = EightBitSpaceShipComplete->RenderTransform.Translation;
	FVector2D End = FVector2D(3000, 0);
	UABTweener_Module::Play(Start, Start - End, [&](FVector2D t) {EightBitSpaceShipComplete->SetRenderTranslation(t); }, 2, ETweenEase::EaseOutQuint)->SetOnCompleted([&](void) {ContinueMoveCPPInfoBox(); });
}

void UVariousMenuWidget::ContinueMoveCPPInfoBox()
{
	EightBitSpaceShipComplete->SetRenderScale(FVector2D(1, 1));
	EightBitSpaceShipComplete->SetRenderTranslation(FVector2D(-1700, 100));

	FVector2D ShipStart = EightBitSpaceShipComplete->RenderTransform.Translation;
	FVector2D InfoStart = EightBitInfoDescriptionText->RenderTransform.Translation;
	FVector2D BackStart = EightBitCPPInfoReturnButton->RenderTransform.Translation;
	FVector2D End = FVector2D(1500, 0);
	UABTweener_Module::Play(ShipStart, ShipStart + End, [&](FVector2D t) {EightBitSpaceShipComplete->SetRenderTranslation(t); }, 2, ETweenEase::EaseOutQuint);
	UABTweener_Module::Play(InfoStart, InfoStart + End, [&](FVector2D y) {EightBitInfoDescriptionText->SetRenderTranslation(y); }, 2, ETweenEase::EaseOutQuint);
	UABTweener_Module::Play(BackStart, BackStart + End, [&](FVector2D u) {EightBitCPPInfoReturnButton->SetRenderTranslation(u); }, 2, ETweenEase::EaseOutQuint);
}
#pragma endregion

#pragma region ReverseInfoBox
void UVariousMenuWidget::StartReverseCPPInfoBox()
{
	EightBitSpaceShipComplete->SetRenderScale(FVector2D(-1, -1));
	Owner->GetWorldTimerManager().SetTimer(Delay, this, &UVariousMenuWidget::ReverseCPPInfoBox, .5f, false);
}

void UVariousMenuWidget::ReverseCPPInfoBox()
{
	FVector2D ShipStart = EightBitSpaceShipComplete->RenderTransform.Translation;
	FVector2D InfoStart = EightBitInfoDescriptionText->RenderTransform.Translation;
	FVector2D BackStart = EightBitCPPInfoReturnButton->RenderTransform.Translation;
	FVector2D End = FVector2D(1500, 0);
	UABTweener_Module::Play(ShipStart, ShipStart - End, [&](FVector2D t) {EightBitSpaceShipComplete->SetRenderTranslation(t); }, 2, ETweenEase::EaseInQuint);
	UABTweener_Module::Play(InfoStart, InfoStart - End, [&](FVector2D y) {EightBitInfoDescriptionText->SetRenderTranslation(y); }, 2, ETweenEase::EaseInQuint);
	UABTweener_Module::Play(BackStart, BackStart - End, [&](FVector2D u) {EightBitCPPInfoReturnButton->SetRenderTranslation(u); }, 2, ETweenEase::EaseInQuint)
		->SetOnCompleted([&](void) {EndReverseCPPInfoBox(); });
}

void UVariousMenuWidget::EndReverseCPPInfoBox()
{
	EightBitSpaceShipComplete->SetRenderScale(FVector2D(.5f, .5f));
	FVector2D ShipStart = EightBitSpaceShipComplete->RenderTransform.Translation;
	FVector2D End = FVector2D(3200, 0);

	UABTweener_Module::Play(ShipStart, ShipStart + End, [&](FVector2D t) {EightBitSpaceShipComplete->SetRenderTranslation(t); }, 2, ETweenEase::EaseInQuint)
		->SetOnCompleted([&](void) {EightBitCPPReverseSpaceShip(); });
}
#pragma endregion

#pragma region ReverseButtons
void UVariousMenuWidget::EightBitCPPReverseSpaceShip()
{
	EightBitSpaceShipComplete->SetRenderScale(FVector2D(-1, -1));
	FVector2D ShipStart = EightBitSpaceShipComplete->RenderTransform.Translation;
	FVector2D End = FVector2D(-1500, 0);
	UABTweener_Module::Play(ShipStart, ShipStart + End, [&](FVector2D t) {EightBitSpaceShipComplete->SetRenderTranslation(t); }, 2, ETweenEase::EaseInOutQuint);

	Owner->GetWorldTimerManager().SetTimer(Delay, this, &UVariousMenuWidget::EightBitCPPReverseBackButton, .5f, false);
}

void UVariousMenuWidget::EightBitCPPReverseBackButton()
{
	FVector2D BackStart = EightBitCPPBackButton->RenderTransform.Translation;
	FVector2D End = FVector2D(-1500, 0);
	UABTweener_Module::Play(BackStart, BackStart + End, [&](FVector2D u) {EightBitCPPBackButton->SetRenderTranslation(u); }, 2, ETweenEase::EaseInOutQuint);

	Owner->GetWorldTimerManager().SetTimer(Delay, this, &UVariousMenuWidget::EightBitCPPReverseInfoButton, .5f, false);		
}

void UVariousMenuWidget::EightBitCPPReverseInfoButton()
{
	FVector2D InfoStart = EightBitCPPInfoButton->RenderTransform.Translation;
	FVector2D End = FVector2D(-1500, 0);
	UABTweener_Module::Play(InfoStart, InfoStart + End, [&](FVector2D y) {EightBitCPPInfoButton->SetRenderTranslation(y); }, 2, ETweenEase::EaseInOutQuint)
		->SetOnCompleted([&](void) {EightBitSpaceShipComplete->SetRenderScale(FVector2D(1, 1)); });
}
#pragma endregion

void UVariousMenuWidget::ResetEightBitButtonPos()
{
	FVector2D Zero = FVector2D(0, 0);
	EightBitCPPBackButton->SetRenderTranslation(Zero);
	EightBitCPPInfoButton->SetRenderTranslation(Zero);
	EightBitCPPInfoButton->SetVisibility(ESlateVisibility::Hidden);
	EightBitCPPBackButton->SetVisibility(ESlateVisibility::Hidden);
}

void UVariousMenuWidget::ResetFire()
{
	SpaceShipFire->ClearActiveTweens();
	EightBitSpaceShipFire->SetRenderTranslation(FVector2D(-3000, -250));
	EightBitSpaceShipNoFire->SetRenderTranslation(FVector2D(-3000, -250));
	EightBitSpaceShipFire->SetRenderScale(FVector2D(2, 2));
	EightBitSpaceShipNoFire->SetRenderScale(FVector2D(2, 2));
	EightBitSpaceShipFire->SetRenderTransformAngle(0);
	EightBitSpaceShipNoFire->SetRenderTransformAngle(0);
}
#pragma endregion