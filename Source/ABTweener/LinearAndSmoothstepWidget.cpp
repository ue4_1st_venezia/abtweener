// Fill out your copyright notice in the Description page of Project Settings.


#include "LinearAndSmoothstepWidget.h"
#include "Components/TextBlock.h"
#include "ABTweener_Module.h"

void ULinearAndSmoothstepWidget::StartTween()
{
	FVector2D FirstStart = FirstText->RenderTransform.Translation;
	FVector2D SecondStart = SecondText->RenderTransform.Translation;
	FVector2D End = FVector2D(800, 0);

	UABTweener_Module::Play(FirstStart, FirstStart + End, [&](FVector2D t) {FirstText->SetRenderTranslation(t); }, Seconds, ETweenEase::EaseLinear);
	UABTweener_Module::Play(SecondStart, SecondStart + End, [&](FVector2D y) {SecondText->SetRenderTranslation(y); }, Seconds, ETweenEase::EaseSmoothstep);
}

void ULinearAndSmoothstepWidget::ResetTranslation()
{
	Super::ResetTranslation();
}
