// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseTweenWidget.h"
#include "LinearAndSmoothstepWidget.generated.h"

/**
 *
 */
UCLASS()
class ABTWEENER_API ULinearAndSmoothstepWidget : public UBaseTweenWidget
{
	GENERATED_BODY()

public:
	virtual void StartTween() override;
	virtual void ResetTranslation() override;
};
