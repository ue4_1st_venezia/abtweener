// Fill out your copyright notice in the Description page of Project Settings.


#include "LoopAndYoyoWidget.h"
#include "Components/TextBlock.h"
#include "ABTweener_Module.h"

void ULoopAndYoyoWidget::StartTween()
{
	FVector2D FirstStart = FirstText->RenderTransform.Translation;
	FVector2D SecondStart = SecondText->RenderTransform.Translation;
	FVector2D ThirdStart = ThirdText->RenderTransform.Translation;
	FVector2D End = FVector2D(800, 0);

	NoDelay->Play(FirstStart, FirstStart + End, [&](FVector2D t) {FirstText->SetRenderTranslation(t); }, Seconds, ETweenEase::EaseLinear)->SetYoyo(true)->SetLoops(3);
	YoyoDelay->Play(SecondStart, SecondStart + End, [&](FVector2D y) {SecondText->SetRenderTranslation(y); }, Seconds, ETweenEase::EaseLinear)->SetYoyo(true)->SetYoyoDelay(3)->SetLoops(3);
	LoopDelay->Play(ThirdStart, ThirdStart + End, [&](FVector2D y) {ThirdText->SetRenderTranslation(y); }, Seconds, ETweenEase::EaseLinear)->SetYoyo(true)->SetLoops(3)->SetLoopDelay(3);

}

void ULoopAndYoyoWidget::ResetTranslation()
{
	Super::ResetTranslation();

	NoDelay->ClearActiveTweens();
	YoyoDelay->ClearActiveTweens();
	LoopDelay->ClearActiveTweens();
}
