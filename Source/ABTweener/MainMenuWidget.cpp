// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuWidget.h"
#include "Components/Image.h"
#include "Components/Button.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "ABTweener_Module.h"

void UMainMenuWidget::NativeConstruct()
{
	Super::NativeConstruct();

	Owner = Cast<APlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));

	InitialSets();

	CPP_Button->OnHovered.AddUniqueDynamic(this, &UMainMenuWidget::CPPButtonHover);
	CPP_Button->OnUnhovered.AddUniqueDynamic(this, &UMainMenuWidget::CPPButtonUnhover);
	CPP_Button->OnClicked.AddUniqueDynamic(this, &UMainMenuWidget::SecondTween);
}

void UMainMenuWidget::InitialSets()
{
	FirstStar->SetVisibility(ESlateVisibility::Hidden);
	SecondStar->SetVisibility(ESlateVisibility::Hidden);
	ThirdStar->SetVisibility(ESlateVisibility::Hidden);
	BoxImage->SetVisibility(ESlateVisibility::Hidden);
	BackButton->SetVisibility(ESlateVisibility::Hidden);
	FirstStar->SetRenderTransformAngle(180);
	SecondStar->SetRenderTransformAngle(215);
	ThirdStar->SetRenderTransformAngle(-180);
	FirstStar->SetRenderScale(FVector2D::ZeroVector);
	SecondStar->SetRenderScale(FVector2D::ZeroVector);
	ThirdStar->SetRenderScale(FVector2D::ZeroVector);
	BoxImage->SetRenderScale(FVector2D(1, 0));
}

void UMainMenuWidget::SecondTween()
{
	FVector2D BPStart = BP_Button->RenderTransform.Translation;
	FVector2D ButtonEnd = FVector2D(0, 550);

	FTimerHandle Delay;

	UABTweener_Module::Play(BPStart, BPStart + ButtonEnd, [&](FVector2D t) {BP_Button->SetRenderTranslation(t); }, 1, ETweenEase::EaseInOutBack);
	Owner->GetWorldTimerManager().SetTimer(Delay, this, &UMainMenuWidget::ContinueSecondTween, .5f, false);
}

void UMainMenuWidget::ContinueSecondTween()
{
	FVector2D CPPStart = CPP_Button->RenderTransform.Translation;
	FVector2D ButtonEnd = FVector2D(0, 550);
	FVector2D TextTranslationStart = ABTweener_Text->RenderTransform.Translation;
	FVector2D TextTranslationEnd = FVector2D(0, 250);
	FVector2D TextScaleStart = ABTweener_Text->RenderTransform.Scale;
	FVector2D TextScaleEnd = FVector2D(1, 1);

	UABTweener_Module::Play(CPPStart, CPPStart + ButtonEnd, [&](FVector2D t) {CPP_Button->SetRenderTranslation(t); }, 1, ETweenEase::EaseInOutBack)->SetOnCompleted([&](void) {A(); });
}

void UMainMenuWidget::A()
{
	FVector2D TextTranslationStart = ABTweener_Text->RenderTransform.Translation;
	FVector2D TextTranslationEnd = FVector2D(0, 250);
	UABTweener_Module::Play(TextTranslationStart, TextTranslationStart + TextTranslationEnd, [&](FVector2D y) {ABTweener_Text->SetRenderTranslation(y); }, 1, ETweenEase::EaseOutBounce)->SetOnCompleted([&](void) {B(); });
}

void UMainMenuWidget::B()
{
	CPP_Button->SetRenderScale(FVector2D(1, 1));
	FVector2D TextScaleStart = ABTweener_Text->RenderTransform.Scale;
	FVector2D TextScaleEnd = FVector2D(1, 1);

	UABTweener_Module::Play(TextScaleStart, TextScaleStart - TextScaleEnd, [&](FVector2D u) {ABTweener_Text->SetRenderScale(u); }, 2, ETweenEase::EaseInBounce)->SetOnCompleted([&](void) {ThirdTween(); });
}

void UMainMenuWidget::ThirdTween()
{
	FirstStar->SetVisibility(ESlateVisibility::Visible);
	SecondStar->SetVisibility(ESlateVisibility::Visible);
	ThirdStar->SetVisibility(ESlateVisibility::Visible);
	BoxImage->SetVisibility(ESlateVisibility::Visible);
	BackButton->SetVisibility(ESlateVisibility::Visible);

	FVector2D BoxStart = BoxImage->RenderTransform.Scale;
	FVector2D BoxEnd = FVector2D(0, 1);

	UABTweener_Module::Play(BoxStart, BoxStart + BoxEnd, [&](FVector2D t) {BoxImage->SetRenderScale(t); }, 1, ETweenEase::EaseLinear)->SetOnCompleted([&](void) {AppairFirstStarTween(); });
}


void UMainMenuWidget::ReverseThirdTween()
{
}

void UMainMenuWidget::AppairFirstStarTween()
{
	FVector2D FSVectorStart = FirstStar->RenderTransform.Scale;
	FVector2D FSVectorEnd = FVector2D(1, 1);
	float FSFloatStart = FirstStar->RenderTransform.Angle;
	float FSFloatEnd = -210;

	UABTweener_Module::Play(FSVectorStart, FSVectorStart + FSVectorEnd, [&](FVector2D t) {FirstStar->SetRenderScale(t); }, 1, ETweenEase::EaseLinear);
	UABTweener_Module::Play(FSFloatStart, FSFloatStart + FSFloatEnd, [&](float y) {FirstStar->SetRenderTransformAngle(y); }, 1, ETweenEase::EaseInOutCirc)->SetOnCompleted([&](void) {C(); });

}

void UMainMenuWidget::C()
{
	AppairSecondStarTween();
	float Start = FirstStar->Percent;
	float End = 1;
	UABTweener_Module::Play(Start, Start + End, [&](float t) {FirstStar->SetPercent(t); }, 1, ETweenEase::EaseLinear);
}

void UMainMenuWidget::ReverseFirstStarTween()
{
}

void UMainMenuWidget::AppairSecondStarTween()
{
	FVector2D SSVectorStart = SecondStar->RenderTransform.Scale;
	FVector2D SSVectorEnd = FVector2D(1, 1);
	float SSFloatStart = SecondStar->RenderTransform.Angle;
	float SSFloatEnd = -215;

	UABTweener_Module::Play(SSVectorStart, SSVectorStart + SSVectorEnd, [&](FVector2D t) {SecondStar->SetRenderScale(t); }, 1, ETweenEase::EaseLinear);
	UABTweener_Module::Play(SSFloatStart, SSFloatStart + SSFloatEnd, [&](float y) {SecondStar->SetRenderTransformAngle(y); }, 1, ETweenEase::EaseInOutCirc)->SetOnCompleted([&](void) {D(); });
}

void UMainMenuWidget::D()
{
	AppairThirdStarTween();
	float Start = SecondStar->Percent;
	float End = 1;
	UABTweener_Module::Play(Start, Start + End, [&](float t) {SecondStar->SetPercent(t); }, 1, ETweenEase::EaseLinear);
}

void UMainMenuWidget::ReverseSecondStarTween()
{
}

void UMainMenuWidget::AppairThirdStarTween()
{
	FVector2D TSVectorStart = ThirdStar->RenderTransform.Scale;
	FVector2D TSVectorEnd = FVector2D(1, 1);
	float TSFloatStart = ThirdStar->RenderTransform.Angle;
	float TSFloatEnd = -210;

	UABTweener_Module::Play(TSVectorStart, TSVectorStart + TSVectorEnd, [&](FVector2D t) {ThirdStar->SetRenderScale(t); }, 1, ETweenEase::EaseLinear);
	UABTweener_Module::Play(TSFloatStart, TSFloatStart + TSFloatEnd, [&](float y) {ThirdStar->SetRenderTransformAngle(y); }, 1, ETweenEase::EaseInOutCirc)->SetOnCompleted([&](void) {C(); });
}

void UMainMenuWidget::E()
{
}

void UMainMenuWidget::ReverseThirdStarTween()
{
}

void UMainMenuWidget::CPPButtonHover()
{
	FVector2D Start = CPP_Button->RenderTransform.Scale;
	FVector2D End = FVector2D(0.3f, 0.3f);
	UABTweener_Module::Play(Start, Start + End, [&](FVector2D t) {CPP_Button->SetRenderScale(t); }, 0.5f, ETweenEase::EaseOutBounce);
}

void UMainMenuWidget::CPPButtonUnhover()
{
	CPP_Button->SetRenderScale(FVector2D(1.0f, 1.0f));
}

void UMainMenuWidget::BackButtonClick()
{
}
