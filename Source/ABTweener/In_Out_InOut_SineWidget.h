// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseTweenWidget.h"
#include "In_Out_InOut_SineWidget.generated.h"

/**
 * 
 */
UCLASS()
class ABTWEENER_API UIn_Out_InOut_SineWidget : public UBaseTweenWidget
{
	GENERATED_BODY()

public:
	virtual void StartTween() override;
	virtual void ResetTranslation() override;	
};
